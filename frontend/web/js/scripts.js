$(document).ready(function () {


    $("#reg_form").hide().css('opacity', 1);

    /*
     |
     |
     |
     18.01.2015 - Scrollbar
     |
     |
     |
     */

    //var opts = $(document).find("#registrationform-nomination_id option");
    //
    //opts.each(function () {
    //    var item = $(this),
    //        cur_text = item.text(),
    //        list = $(".select_list");
    //    //item_ind = item.index();
    //
    //    list.append("<li>" + cur_text + "</li>");
    //});

    $(".list").mCustomScrollbar({
        scrollbarPosition: "inside",
        scrollInertia: 100,
        setTop: '0px'
        //alwaysShowScrollbar:2
    });

    //$(".nomi_select .list").hide();
    //
    //$(document).on("click", ".select_list li", function () {
    //    //$("#registrationform-nomination_id").val($(this).text());
    //    var seelcted_val = $(this).text();
    //    $("#registrationform-nomination_id option").each(function () {
    //        var item = $(this);
    //        if (seelcted_val == $(item).text()) {
    //            item.prop('selected', true);
    //        }
    //    });
    //
    //    checkForm();
    //
    //    $(".nomi_select .select").text($(this).text());
    //    $(".nomi_select .list").slideUp();
    //    $(".nomi_select .select").removeClass('active');
    //});

    //$(document).on('click', ".nomi_select .select", function () {
    //    if (!$(this).hasClass('active')) {
    //        $(".nomi_select .list").slideDown();
    //        $(this).addClass('active');
    //    }
    //    else {
    //        $(".nomi_select .list").slideUp();
    //        $(this).removeClass('active');
    //    }
    //});

    /*
     |
     |
     |
     | Scrollbar end
     |
     |
     |
     */

    var r_h = $(".main-description .right").height();
    $(".main-description .left").height(r_h);

    $(window).on('resize', function() {
        var r_h = $(".main-description .right").height();
        $(".main-description .left").height(r_h);
        //$(".vid_gallery .list").height(r_h);
    });












    // Видеогалерея

    var h_player = $(".player").height(),
        scrl_hg = $(".left").height() - h_player,
        g_list = $(".vid_gallery .list"),
        play_btn = $(".player .play");
    play_cont = $(".player .cont")

    var videomode = 0;


    $(window).on('resize', function() {

        var r_h = $(".main-description .right").height();
        $(".main-description .left").height(r_h);

        var h_player = $(".player").height(),
            scrl_hg = $(".main-description").height() - h_player,
            g_list = $(".vid_gallery .list"),
            play_btn = $(".player .play");

        $(".vid_gallery .list").height(scrl_hg+24);

        if (play_cont.has('iframe')) {
            $(".player .cont iframe").width(play_cont.width());
            $(".player .cont iframe").height(play_cont.height());
        }

        g_list.mCustomScrollbar({
            theme: "dark-2"
        });
    });

    g_list.height(scrl_hg + 24);

    g_list.mCustomScrollbar({
        theme: "dark-2"
    });

    play_btn.fadeOut(10);

    g_list.find(".item").on('click', function() {
        //$(".vid_gallery .list .item").each(function() {
        //    $(this).removeClass(".selected");
        //});
		$(this).addClass("selected").siblings(".item").removeClass("selected");

        var curattr = $(this).html(),
            vid = $(this).hasClass("video");

        if (vid) {
            play_btn.fadeIn(600);
            videomode = 1;
        }
        else {
            play_btn.fadeOut(600);
            videomode = 0;
        }

        play_cont.fadeOut(200, function() {
            $(this).html(curattr).fadeIn(200);
        });
    });

    play_btn.on('click', function() {
        if (videomode) {
            link = play_cont.find("img").attr("video-href");
            iframe_string = '<iframe width="' + play_cont.width() + '" height="' + play_cont.height() + '" src="' + link + '" frameborder="0" allowfullscreen></iframe>';
            play_cont.fadeOut(200, function() {
                play_btn.fadeOut(200);
                $(this).html(iframe_string).fadeIn(3000);
            });
        }
    });











    var	f_button = $(".open_r_form"),
        r_form = $("#reg_form"),
        main_l = $(".main-description .left"),
        main_r = $(".main-description .right");

    f_button.on('click', function() {

        r_form.slideToggle().css({opacity:1});

    });

    // Слайдеры

    var main_news = $(".news-slider .slider"),
        main_jury = $(".jury-slider .slider");


    $(main_news).slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        lazyLoad: false,
        swipe: true,
        draggable: true,
        prevArrow: ".button.left",
        nextArrow: ".button.right",
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });

    $(main_jury).slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        lazyLoad: false,
        swipe: true,
        draggable: true,
        prevArrow: ".button.left",
        nextArrow: ".button.right",
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 2
                }
            }
        ]
    });



    $(document).on('change', '#reg_form .form-control', function () {
        checkForm();
        //alert(index);
    });

    //	Update Captcha
    $(document).on('click', '.cap_upd', function () {
        $(document).find('#registrationform-verifycode-image').click();
    });

    // Ajax submit form
    $(document).on('click', '.row button.reg', function () {
        console.log('button click');
        if (!$(this).hasClass('inactive')) {
            console.log('button ajax submit');
            $.ajax({
                type: 'post',
                url: $('#reg_form').attr('action'),
                data: $('#reg_form').serialize(),
                dataType: 'json',
                success: function (json) {
                    console.log("Html: "+json.html);
                    console.log("Error: "+json.error);
                    if (json.error) {
                        $('#reg_form').html('');
                        $('#reg_form').html(json.html);
                    } else {
                        var html  = json.html;

                        $('#reg_form').animate({
                            opacity: 0,
                            height: "toggle"
                        }, 1000).before(html);

                    }
                }
            });
        }
        return false;

    });

    var string = location.search,
        param = '',
        par = 'registration';

    if (string != '') {
        param = string.slice(1).split('=')[1];
    }

    if (param == par) {
        $('html, body').animate({
            scrollTop: $(".open_r_form").offset().top
        }, 1000, function () {
            $("#reg_form").slideDown();
        });

        console.log("String: '" + string + "'; Param: '" + param + "'");
        console.log("Got 'registration' param");
        console.log($("#reg_form").offset().top);
    }

    //#####################################################################

    // Share buttons
    $('.share .yandex-share').each(function () {
        shareButton(this)
    });

    $(document).ajaxComplete(function () {
        $('.share .yandex-share').each(function () {
            shareButton(this)
        });

        opts.each(function () {
            var item = $(this),
                cur_text = item.text(),
                list = $(".select_list");
            //item_ind = item.index();

            list.append("<li>" + cur_text + "</li>");
        });

        $(".list").mCustomScrollbar({
            scrollbarPosition: "inside",
            scrollInertia: 100,
            setTop: '0px'
            //alwaysShowScrollbar:2

        });

        $(".nomi_select .list").hide();

    });

});

function shareButton(_this) {
    var title = $(_this).parent('.share').attr('post-title');
    var link = $(_this).parent('.share').attr('post-http');
    var id = $(_this).attr('id');
    new Ya.share({
        element: id,
        elementStyle: {
            'type': 'small',
            'border': false,
            'quickServices': ['vkontakte', 'facebook', 'twitter', 'linkedin']
        },
        theme: 'counter',
        link: link,
        title: title,
        popupStyle: {
            blocks: {
                'Поделись-ка!': ['vkontakte', 'facebook', 'twitter', 'linkedin'],
                'Поделись-ка по-другому!': ['vkontakte', 'facebook', 'twitter', 'linkedin']
            },
            copyPasteField: true
        }
    });
}

function checkForm() {
    var index = 0;
    var min_count = 6;
    $('#reg_form .form-group input[type="text"]').each(function() {
        if ($(this).val() != "") {
            index = index + 1;
        }
    });

    //if ($("#registrationform-nomination_id option:selected").val()) {
    //    index = index + 1;
    //}

    if (index >= min_count) {
        $('.row button.reg').removeClass('inactive');
    } else {
        $('.row button.reg').addClass('inactive');
    }
}
