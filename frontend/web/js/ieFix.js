$(document).ready(function() {
    
    var gal_item = $(".vid_gallery").find(".item"),
        vid_item = $(".vid_gallery").find(".video");
    
    gal_item.each(function() {
        $(this).append("<div class='ie_fix_overlay'></div>");
    });
    
    vid_item.each(function() {
        $(this).append("<div class='ie_fix_playbtn'></div>");
    });
    
});