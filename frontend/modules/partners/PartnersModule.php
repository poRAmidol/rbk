<?php

namespace frontend\modules\partners;

use common\componets\myModule;
use common\modules\appmodule\models\Module;
use common\modules\menu\models\Menu;

class PartnersModule extends myModule
{
    public $controllerNamespace = 'frontend\modules\partners\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function rules () {

        $module = Module::find()->where('module_name = :name', [':name' => "partners"])->one();
        $menuArr = Menu::find()->where('module_id = :m_id', ['m_id' => $module->id])->all();

        $url = "";
        $ruleArr = [];
        foreach ($menuArr as $item) {
            $ruleArr['<menu_url:('.str_replace("/", "\/",$item->url).')>'] = 'partners/default/index';
        }
        return $ruleArr;
    }
}
