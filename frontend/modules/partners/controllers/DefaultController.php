<?php

namespace frontend\modules\partners\controllers;

use common\modules\menu\models\Menu;
use common\modules\partners\models\PartnersCategory;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex($menu_url)
    {
        $menu = Menu::find()->where('url = :url', [':url' => $menu_url])->one();
        $categories = PartnersCategory::find()->orderBy('position ASC')->all();

        return $this->render('index', ['categories' => $categories, 'menu' => $menu]);
    }
}
