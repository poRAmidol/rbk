<?php
use yii\helpers\Html;


$this->title = $menu->seo_title." - ".Yii::$app->params['seo_title'];
Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $menu->seo_keywords]);
Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $menu->seo_description]);
?>
<section class="partners">
    <div class="container">
        <div class="row">
            <?php
            foreach ($categories as $cat) {

                if (!empty($cat->partners)) {
                    echo Html::tag('h1', $cat->name);
                }

                foreach ($cat->partners as $partner) {

                    ?>
                    <div class="col-md-6">
                        <a name="<?=\common\helpers\CString::translitTo($partner->name)?>"></a>
                        <div class="logo">
                            <?php
                            if ($partner->logo != "") {
                                echo Html::a(Html::img("/".$partner->logo_src."/".$partner->logo), (($partner->url != "") ? $partner->url : null), ['target' => '_blank']);
                            }
                            ?>
                        </div>
                        <h2><?=$partner->name?></h2>
                        <?=$partner->description?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</section>