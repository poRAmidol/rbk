<?php
/**
 * Created by PhpStorm.
 * User: pashaevs
 * Date: 29.01.15
 * Time: 22:58
 */

namespace frontend\modules\partners\widgets;

use yii\base\Widget;
use common\modules\partners\models\PartnersCategory;

class MainPagePartnersWidget extends Widget
{

    public function run()
    {
        $showCatName = false;
        $i = 0;
        $categories = PartnersCategory::find()->orderBy('position ASC')->all();
        foreach ($categories as $cat) {
            if (!empty($cat->partners)) {
                $i = $i + 1;
            }
        }
        $showCatName = ($i > 1 ? true : false);


        return $this->render("_main_page_partners", ['categories' => $categories, 'showCatName' => $showCatName]);
    }
}