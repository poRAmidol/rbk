<?php
/**
 * Created by PhpStorm.
 * User: pashaevs
 * Date: 29.01.15
 * Time: 22:59
 */
if (!empty($categories)) {
    ?>
    <section class="main-partners">
        <div class="container">
            <div class="row">
                <p>
                    <a href="<?= \yii\helpers\Url::to(['/partners']) ?>" class="h1">Партнеры</a>
                </p>
                <?php
                foreach ($categories as $index => $cat) {
                    if (!empty($cat->partners)) {
                        if ($showCatName) {
                            echo \yii\helpers\Html::tag('h3', $cat->name);
                        }
                        echo '<div class="row">';
                        foreach ($cat->partners as $partner) {
                            if ($partner->logo != "" && is_file(Yii::$app->basePath ."/../". $partner->logo_src . "/" . $partner->logo)) {
                                ?>
                                <div class="col-xs-6 col-sm-3 col-md-3">
                                    <a href="<?= \yii\helpers\Url::to(['/partners', '#' => \common\helpers\CString::translitTo($partner->name)]) ?>"><?= \yii\helpers\Html::img("/" . $partner->logo_src . "/" . $partner->logo)?></a>
                                </div>
                            <?php
                            }
                        }
                        echo '</div>';
                    }
                }
                ?>
            </div>
        </div>
    </section>
<?php
}
?>