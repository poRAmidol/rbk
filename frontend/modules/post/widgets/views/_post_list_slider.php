<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 23.06.14
 * Time: 20:14
 */
use yii\helpers\Html;

if (!empty($post_list)) {
    ?>
    <section class="news-slider">
        <div class="container">
            <a href="<?= \yii\helpers\Url::to(["/".$menu->url]) ?>" class="h1">Новости</a>

            <div class="row">
                <div class="button left"><img src="/img/icons/slider_prev.png"/></div>
                <div class="slider">
                    <?php
                    foreach ($post_list as $_post) {
                        ?>
                        <div class="col-xs-12 col-sm-6 col-md-6 item">

                            <h3><?= Html::a($_post->title, ['/' . $menu->url . '/' . $_post->id . "_" . $_post->alt_title, '#' => $_post->id ]) ?></h3>
                            <h5><?= Yii::$app->formatter->asDate($_post->date, "d MMMM") ?></h5>
                            <p><?= strip_tags($_post->small_text) ?></p>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="button right"><img src="/img/icons/slider_next.png"/></div>
            </div>
        </div>
    </section>
<?php
}
?>
