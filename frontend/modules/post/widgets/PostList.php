<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 23.06.14
 * Time: 20:11
 */
namespace frontend\modules\post\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;

use common\modules\post\models\Post;

class PostList extends Widget {

    public $menu_id = null;
    public $limit = 10;
    public $template = "_post_list_slider";


    public function run() {

        if ($this->menu_id == null)
            throw new InvalidConfigException('Параметр menu_id пуст');

        $menu = \common\modules\menu\models\Menu::find()->where(['id' => $this->menu_id])->one();
        if (empty($menu))
            return null;

        $_list = Post::find()
            ->where(['menu_id' => $menu->id])
            ->orderBy('date DESC')
            ->limit($this->limit)
            ->all();

        return $this->render($this->template, ['post_list' => $_list, 'menu' => $menu]);
    }


}