<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\modules\post\models\Post $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<!--<section class="widget">-->
    <div class="row news-form">

        <?php $form = ActiveForm::begin([
            'options' => [
                'novalidate' => "novalidate",
                'method' => "post",
                'data-validate' => "parsley"
            ]
        ]); ?>

<!--        --><?//= $form->field($model, 'publish')->checkbox(); ?>
        <div style="width: 15%;">
            <?= $form->field($model, 'date')->widget(\kartik\widgets\DatePicker::className(), [
                'size' => 'sx'
            ]); ?>
        </div>

        <?= $form->field($model, 'author')->textInput(); ?>
        <?= $form->field($model, 'source')->textInput(); ?>

        <?= $form->field($model, 'menu_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\modules\menu\models\Menu::find()->where('module_id = 1')->all(), 'id', 'name'), ['prompt' => '---']) ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => 512]) ?>

        <?= $form->field($model, 'small_text', [
            'template' => "
                {label}
                <div style='color:#000;'>
                {input}
                </div>
                {error}
            "
        ])->widget(sim2github\imperavi\widgets\Redactor::className(), [
            'options' => [
                'debug' => 'true',
            ],
            'clientOptions' => [ // [More about settings](http://imperavi.com/redactor/docs/settings/)
//                'buttonSource' => true,
                'cleanOnPaste' => false,
                'lang' => 'ru',
                'cleanOnPaste' => true,
                'cleanSpaces' => false,
                'paragraphize' => true,
                'removeEmpty' => ['strong', 'b'],
//                'allowedTags' => ['p', 'blockquote', 'br', 'b', 'strong', 'i', 'ul', 'li', 'ol', 'a', 'div', 'span', 'bold', 'table', 'tr', 'td', 'thead', 'tbody', 'tfoot', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
                'pastePlainText' => false,
                'replaceDivs' => false,
                'imageGetJson' =>  \Yii::getAlias('@web').'/redactor/upload/imagejson', //By default
                'plugins' => [ // [More about plugins](http://imperavi.com/redactor/plugins/)
                    'video'
                ]
            ],

        ])
        ?>

        <?= $form->field($model, 'text', [
            'template' => "
                {label}
                <div style='color:#000;'>
                {input}
                </div>
                {error}
            "
        ])->widget(sim2github\imperavi\widgets\Redactor::className(), [
            'options' => [
                'debug' => 'true',
            ],
            'clientOptions' => [ // [More about settings](http://imperavi.com/redactor/docs/settings/)
                'buttonSource' => true,
                'cleanOnPaste' => false,
                'lang' => 'ru',
                'cleanOnPaste' => true,
                'cleanSpaces' => false,
                'paragraphize' => true,
                'removeEmpty' => ['strong', 'b'],
//                'allowedTags' => ['p', 'blockquote', 'br', 'b', 'strong', 'i', 'ul', 'li', 'ol', 'a', 'div', 'span', 'bold', 'table', 'tr', 'td', 'thead', 'tbody', 'tfoot', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
                'pastePlainText' => false,
                'replaceDivs' => false,
                'imageGetJson' =>  \Yii::getAlias('@web').'/redactor/upload/imagejson', //By default
                'plugins' => [ // [More about plugins](http://imperavi.com/redactor/plugins/)
                    'video'
                ]
            ],

        ])
        ?>

        <fieldset>
            <legend>SEO Атрибуты</legend>
            <?= $form->field($model, 'seo_title')->textInput(['maxlength' => 512]) ?>

            <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 512]) ?>

            <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>
        </fieldset>


        <div class="form-actions">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'name' => 'submit', 'value' => 'submit']) ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;или&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?= Html::submitButton('Опубликовать', ['class' => 'btn btn-danger', 'name' => 'submit', 'value' => 'publish']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
<!--</section>-->

