<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 24.06.14
 * Time: 20:16
 */

if ($new != null) {
    $this->title = $new->title . " - " . $menu->seo_title . " - " . Yii::$app->params['seo_title'];
} else {
    $this->title = $menu->seo_title . " - " . Yii::$app->params['seo_title'];
}


Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $menu->seo_keywords]);
Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $menu->seo_description]);
/*

*/
?>


<!-- Блок новостей -->
<section class="news">
    <div class="container">
        <div id="totalPosts" style="display: none;"><?= $count ?></div>
        <?php
        \yii\widgets\Pjax::begin([
            'scrollTo' => true,
            'linkSelector' => 'a[data-pjax="1"]'
        ]);


        foreach ($models as $model) {
            ?>
            <?php

            \common\widget\html\ActiveEdit::begin(['model' => $model, 'url' => "/post/simple-edit/edit"]);


            ?>

            <!-- Блок краткого описания новости -->
            <div class="news_preview">
                <div class="row">

                    <div class="col-md-9">
                        <a name="<?= $model->id ?>"></a>
                        <!-- Заголовок -->
                        <h1><?= $model->title ?></h1>

                        <!-- Дата, автор, источник -->
                        <div class="meta">
                            <span class="date"><?= Yii::$app->formatter->asDate($model->date, "d MMMM Y") ?></span>
                            <span class="author"><?= $model->author ?></span>
                            <?= \yii\helpers\Html::a($model->source, 'http://' . $model->source, ['class' => 'source']) ?>
                            <!--                <a href="" class="source">advertology.ru</a>-->
                        </div>
                    </div>

                    <!-- Описание новости -->
                    <div class="col-md-9 n_info">

                        <!-- Краткий текст новости -->
                        <?php


                        if ($new != null) {
                            if ($new->id == $model->id) {
                                ?>
                                <div>
                                    <div class="container-text">
                                        <?= $model->text ?>
                                    </div>

                                    <!--                        <p class="white-gradient"> </p>-->
                                    <br/>
                                </div>
                                <?= \yii\helpers\Html::a("Свернуть", ['/' . $menu->url, '#' => $model->id], ['class' => 'read_full hide-news', 'data-pjax' => 0]); ?>
                            <?php
                            } else {
                                ?>
                                <div>
                                    <div class="container-text">
                                        <p><?= strip_tags($model->small_text) ?></p>
                                        <div class="white-gradient"> </div>
                                    </div>

                                    <br/>
                                </div>
                                <?= \yii\helpers\Html::a("Читать полностью", ['/' . $menu->url . '/' . $model->id . "_" . $model->alt_title, '#' => $model->id], ['class' => 'read_full', 'data-pjax' => 1]); ?>
                            <?php
                            }
                        } else {
                            ?>
                            <div>
                                <div class="container-text">
                                    <p><?=strip_tags($model->small_text) ?></p>
                                    <div class="white-gradient"> </div>
                                </div>

                                <br/>
                            </div>
                            <?= \yii\helpers\Html::a("Читать полностью", ['/' . $menu->url . '/' . $model->id . "_" . $model->alt_title, '#' => $model->id], ['class' => 'read_full', 'data-pjax' => 1]); ?>
                        <?php
                        }
                        ?>
                        <!-- Кнопки шаринга в соцсетях -->
                        <div class="share" style="display: inline-block; height: 30px; margin-left: 20px;"
                             post-title="<?= $model->title ?>"
                             post-http="http://<?= $_SERVER['HTTP_HOST'] . \yii\helpers\Url::to(['/' . $menu->url . '/' . $model->id . "_" . $model->alt_title]); ?>">
                            <span id="ya_share<?= $model->id ?>" class="yandex-share"></span>
                        </div>

                    </div>

                    <!-- Блок картинки-превью -->
                    <div class="col-md-3 n_img">
                        <?php
                        //                $matches = [];
                        //                preg_match_all('/<img[^>]*src="([^"]*)"/i', $model->text, $matches);
                        //                if (!empty($matches))
                        //                    for ($i = 0; $i < 1; $i++) {
                        //                        if (isset($matches[1][$i]))
                        //                            echo \yii\helpers\Html::img($matches[1][$i], ['style' => 'width:240px;']);
                        //                    }
                        //
                        ?>
                    </div>
                </div>
            </div>

            <?php
            \common\widget\html\ActiveEdit::end();
            ?>
        <?php
        }

        \yii\widgets\Pjax::end()
        ?>
        <div id="post_bottom"></div>
    </div>
</section>