<?php

//$this->title = $model->title . " - " . $menu->seo_title . " - " . Yii::$app->params['seo_title'];
//Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $model->seo_keywords]);
//Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $model->seo_description]);
?>




<?php
//\common\widget\html\ActiveEdit::begin(['model' => $model, 'url' => "/post/simple-edit/edit"]);
?>
<article class="nominees">

    <div class="container">
        <a href="<?= \yii\helpers\Url::toRoute("/" . $menu->url) ?>" class="back-link">&larr; <?= $menu->name ?></a>
        <!-- Кнопки шаринга в соцсетях -->

        <div class="news_preview" style="padding: 0; margin: 0;">
            <div class="meta">
                <span class="date"><?=Yii::$app->formatter->asDate($model->date, "d MMMM Y")?></span>
                <span class="author"><?=$model->author?></span>
                <?=\yii\helpers\Html::a($model->source, 'http://'.$model->source, ['class' => 'source'])?>
                <!--                <a href="" class="source">advertology.ru</a>-->
            </div>
        </div>
        <h1><?= $model->title ?></h1>

        <div class="text-content">
            <?= $model->text ?>
        </div>


        <div class="share" style="display: block; margin-top: 40px; text-align: right; ">
            <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,linkedin" data-yashareTheme="counter"></div>
        </div>
    </div>

</article>

<?php
//\common\widget\html\ActiveEdit::end();
?>

