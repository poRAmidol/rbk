<?php
use yii\helpers\Html;
//use yii\widgets\Breadcrumbs;
//use frontend\assets\AppAsset;
//use frontend\widgets\Alert;



//AppAsset::register($this);


//$this->registerAssetBundle('frontend\assets\AppAsset', \yii\web\View::POS_HEAD);
/**
 * @var yii\web\View $this
 * @var common\modules\post\models\Post $model
 */

?>
<div class="news-update padding020 widget">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= \frontend\widgets\Alert::widget() ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>



