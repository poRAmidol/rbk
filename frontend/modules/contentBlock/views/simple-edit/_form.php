<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\contentBlock\models\ContentBlock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-block-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'content', [
        'template' => "
                {label}
                <div style='color:#000;'>
                {input}
                </div>
                {error}
            "
    ])->widget(sim2github\imperavi\widgets\Redactor::className(), [
        'options' => [
            'debug' => 'true',
        ],
        'clientOptions' => [ // [More about settings](http://imperavi.com/redactor/docs/settings/)
            //'convertImageLinks' => 'false', //By default
            //'convertVideoLinks' => 'false', //By default
            'buttonSource' => true,
            //'wym' => 'true',
            //'air' => 'true',
            //'linkEmail' => 'true', //By default
            'cleanOnPaste' => true,
            'cleanSpaces' => false,
            'lang' => 'ru',
            'paragraphize' => true,
            'removeEmpty' => false,
//            'linebreaks' => true,
            'allowedTags' => ['p', 'blockquote', 'br', 'b', 'strong', 'i', 'ul', 'li', 'ol', 'a', 'div', 'span', 'bold', 'table', 'tr', 'td', 'thead', 'tbody', 'tfoot', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
            'pastePlainText' => false,
            'replaceDivs' => false,
//            'convertDivs' => false,
//            'deniedTags' => false,
            //'imageGetJson' =>  \Yii::getAlias('@web').'/redactor/upload/imagejson', //By default
            'plugins' => [ // [More about plugins](http://imperavi.com/redactor/plugins/)
//                'ace',
//                'clips',
//                'fullscreen'
            ]
        ],

    ])
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'name' => 'submit', 'value' => 'submit']) ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;или&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?= Html::submitButton('Опубликовать', ['class' => 'btn btn-success', 'name' => 'submit', 'value' => 'publish']) ?>
        <!--        --><? //= Html::a('Отмена', 'javascript:;', ['class' => 'btn btn-danger ', 'name' => 'cancel', 'title' => 'Close', 'value' => 'cancel' , 'onclick' => "location.reload();"]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
