<?php
/**
 * Created by PhpStorm.
 * User: pashaevs
 * Date: 29.01.15
 * Time: 23:35
 */


if (!empty($stages)) {
    ?>
    <section class="levels">
        <div class="container">

            <h1>Этапы 2015</h1>

            <div class="row">
                <?php
                foreach ($stages as $index => $stage) {
                    ?>
                    <div
                        class="col-xs-6 col-sm-4 col-md-3 level <?= $stage->past_stage == 1 ? "disabled" : "" ?> <?= $stage->current_stage == 1 ? "current" : "" ?>">
                        <h2><?= $stage->number ?></h2>
                        <span class="date <?= (count($stages) == ($index + 1) ? "t-g" : "") ?>"><?= $stage->date ?></span>
                        <?=\yii\helpers\Html::a($stage->title, (($stage->url != "") ? $stage->url : null), ['class' => 'title'.($stage->url == "" ? " removeunderline" : "")])?>
                        <span class="stat"><?= ($stage->past_stage == 1 ? "Этап завершен" : $stage->note) ?></span>
                    </div>
                <?php
                }
                ?>
            </div>

        </div>
    </section>
<?php
}
?>