<?php
/**
 * Created by PhpStorm.
 * User: pashaevs
 * Date: 29.01.15
 * Time: 23:32
 */

namespace frontend\modules\stage\widgets;

use yii\base\Widget;

class MainPageStageWidget extends Widget
{
    public function run()
    {
        $stages = \common\modules\stage\models\Stage::find()->all();

        return $this->render('_main_page_stage', ['stages' => $stages]);
    }
}