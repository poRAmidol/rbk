<?php
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'action' => \yii\helpers\Url::home(),
    'id' => 'reg_form',
//    'validateOnBlur' => true,
//    'enableAjaxValidation' => true,
//    'enableClientValidation' => true,
]); ?>

<?php

if ($emailSendError) {
?>
    <p>
        Ошибка регистрации попробуйте зарегистрироваться через несколько минут.
    </p>
<?php
}
?>

<?=$form->errorSummary($regForm);?>

<?//=$form->field($regForm, 'seminar', [
//    'template' => '
//                <div class="row">
//                    <div class="col-xs-12 сol-sm-6 col-md-4">{label}</div>
//                    <div class="col-xs-12 сol-sm-6 col-md-8">
//                        {input}
//                        <label for="registrationform-seminar"></label>
//                        {error}
//                        {hint}
//                    </div>
//                </div>
//            '
//])->checkbox([], false)?>

<?//=$form->field($regForm, 'brand_name', [
//    'template' => '
//                <div class="row">
//                    <div class="col-xs-12 col-sm-6 col-md-4">{label}</div>
//                    <div class="col-xs-12 col-sm-6 col-md-8">{input}{error}
//                    {hint}</div>
//
//                </div>
//            '
//])->textInput()?>


<?//=$form->field($regForm, 'nomination_id', [
//            'template' => '
//                <div class="row">
//                    <div class="col-xs-12 col-sm-6 col-md-4">{label}</div>
//                    <div class="col-xs-12 col-sm-6 col-md-8">{input}
//                    <div class="nomi_select">
//                        <div class="select">
//                            – – – – – –
//                        </div>
//                        <div class="list">
//                            <ul class="select_list">
//
//                            </ul>
//                        </div>
//                    </div>
//                    {error}
//                    {hint}
//                    </div>
//                </div>
//            '
//        ])->dropDownList(\yii\helpers\ArrayHelper::map(\common\modules\nomination\models\Nomination::find()->all(), 'id', "name"), ['prompt' => ''])?>



<!--    <p>Представитель компании</p>-->

<?=$form->field($regForm, 'first_name', [
    'template' => '
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">{label}</div>
                    <div class="col-xs-12 col-sm-6 col-md-8">{input}{error}
                    {hint}</div>
                </div>
            '
])->textInput()?>

<?=$form->field($regForm, 'last_name', [
    'template' => '
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">{label}</div>
                    <div class="col-xs-12 col-sm-6 col-md-8">{input}{error}
                    {hint}</div>
                </div>
            '
])->textInput()?>

<?=$form->field($regForm, 'job', [
    'template' => '
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">{label}</div>
                    <div class="col-xs-12 col-sm-6 col-md-8">{input}{error}
                    {hint}</div>
                </div>
            '
])->textInput()?>

<?=$form->field($regForm, 'phone', [
    'template' => '
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">{label}</div>
                    <div class="col-xs-12 col-sm-6 col-md-8">{input}{error}
                    {hint}</div>
                </div>
            '

])->textInput([
    'placeholder' => '+7 (495) 363 1111'
])?>

<?=$form->field($regForm, 'email', [
    'template' => '
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">{label}</div>
                    <div class="col-xs-12 col-sm-6 col-md-8">{input}{error}
                    {hint}</div>
                </div>
            ',

])->textInput([
    'placeholder' => 'user@server.com'
])?>


<?= $form->field($regForm, 'verifyCode',[
    'template' => '
                <div class="row captcha">
                    <div class="col-xs-12 col-sm-6 col-md-4">{label}</div>
                    <div class="col-xs-12 col-sm-6 col-md-8">
                        {input}
                        {error}
                        {hint}
                        <div class="cap_upd">
                            <img src="/img/icons/captcha.png"/> <span>Обновить картинку</span>
                        </div>

                    </div>
                </div>',
])->widget(\yii\captcha\Captcha::className(), [

    'imageOptions' => [
        'style' => 'height:32px;'
    ],
    'captchaAction' => 'default/captcha',
]); ?>

    <div class="row">
        <div class="col-md-4"><button class="reg <?=($regForm->first_name == "" ? "inactive" : "")?>">Зарегистрироваться</button></div>
    </div>
<?php ActiveForm::end(); ?>