<?php
use \yii\helpers\Html;


/**
 * Created by PhpStorm.
 * User: developer
 * Date: 16.05.14
 * Time: 12:01
 */


$this->title = Yii::$app->params['seo_title'];
Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['seo_keywords']]);
Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['seo_description']]);
?>

<!-- Описание -->
<section class="main-description">

    <div class="left">
        <?=\frontend\modules\gallery\widgets\GalleryWidget::widget();?>
    </div>

    <div class="right">
        <div class="big_y">2014</div>

        <?= \common\modules\contentBlock\widget\ContentBlockWidget::widget([
            'id' => 4
        ]); ?>
        <br/>


        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <? /*<a class="reg open_r_form">Регистрация</a> */ ?>
            </div>

            <div class="col-lg-9 col-md-9 col-sm-9">
                <?php echo \common\modules\contentBlock\widget\ContentBlockWidget::widget([
                    'id' => 11
                ]); ?>
            </div>
        </div>


        <?= $this->render('_reg_form', ['regForm' => $regForm, 'emailSendError' => false]) ?>


    </div>

</section>

<!-- Слайдер новостей -->
<?= \frontend\modules\post\widgets\PostList::widget([
    'menu_id' => 4,
    'limit' => 10,
    'template' => '_post_list_slider'
]) ?>



<!-- О премии -->
<section class="main-about">
    <div class="container">
        <div class="row">
            <div class="col-xs-7 col-sm-8 col-md-9">
                <?= \common\modules\contentBlock\widget\ContentBlockWidget::widget([
                    'id' => 3
                ]); ?>

            </div>
            <div class="col-xs-5 col-sm-4 col-md-3">
                <SCRIPT language="JavaScript" src="http://www.rbc.ru/bannerjvs/brand_left"></SCRIPT>
                <SCRIPT language="JavaScript"> ShowRBCBanner(); </SCRIPT>
            </div>
        </div>

    </div>
</section>

<!-- Этапы конкурса -->
<?= \frontend\modules\stage\widgets\MainPageStageWidget::widget(); ?>

<!-- Слайдер жюри -->
<?= \frontend\modules\jury\widgets\MainPageJuryWidget::widget(); ?>


<!-- Номинации -->
<section class="main-about">
    <div class="container">
        <div class="row">

            <div class="col-xs-7 col-sm-8 col-md-9">
                <?= \common\modules\contentBlock\widget\ContentBlockWidget::widget([
                    'id' => 5
                ]); ?>
            </div>
            <div class="col-xs-5 col-sm-4 col-md-3">
                <!--
                <SCRIPT language="JavaScript" src="http://www.rbc.ru/bannerjvs/awards_top"></SCRIPT>
                <SCRIPT language="JavaScript"> ShowRBCBanner(); </SCRIPT>
                -->
            </div>

        </div>
    </div>
</section>

<!-- Партнеры -->
<?= \frontend\modules\partners\widgets\MainPagePartnersWidget::widget() ?>

<!-- Контакты -->
<section class="main-contacts">
    <div class="container">
        <?= \common\modules\contentBlock\widget\ContentBlockWidget::widget([
            'id' => 7
        ]); ?>
    </div>
</section>