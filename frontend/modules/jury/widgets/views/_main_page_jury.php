<?php
/**
 * Created by PhpStorm.
 * User: Rashid Abdulaev
 * Date: 29.01.15
 * Time: 23:21
 */

use yii\helpers\Html;

if (!empty($juryArr)) {
    ?>
    <section class="jury-slider">
        <div class="container">
            <a href="<?= \yii\helpers\Url::to([$urlToPage]) ?>" class="h1">Жюри конкурса</a>
            <div class="row">
                <div class="button left"><img src="/img/icons/slider_prev.png"/></div>
                <div class="slider">
                    <?php
                    foreach ($juryArr as $_jury) {
                        ?>
                        <div class="col-xs-6 cols-sm-4 col-md-3 item">
                            <a href="<?=\yii\helpers\Url::to(['/jury', '#' => \common\helpers\CString::translitTo($_jury->flp)])?>" style="color:#000;">
                                <?= \yii\helpers\Html::img("/" . $_jury->img, ['alt' => $_jury->flp]) ?>
                                <?php
                                $flpExplode = explode(" ", $_jury->flp);
                                echo "</br><span>" . implode("</span><br><span>", $flpExplode) . "</span>";
                                ?>
                            </a>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="button right"><img src="/img/icons/slider_next.png"/></div>
                <div class="col-md-4 all">
                    <?php
                    echo \yii\helpers\Html::a('Весь состав жюри', [$urlToPage], ['class' => 'reg']);
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php
} else {

}
?>