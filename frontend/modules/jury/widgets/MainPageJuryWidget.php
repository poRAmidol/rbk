<?php
/**
 * Created by PhpStorm.
 * User: pashaevs
 * Date: 29.01.15
 * Time: 23:21
 */

namespace frontend\modules\jury\widgets;

use yii\base\Widget;
use common\modules\jury\models\Jury;

class MainPageJuryWidget extends Widget
{

    public function run()
    {
        $juryArr = Jury::find()->orderBy('position ASC')->all();

        $_menu = \common\modules\menu\models\Menu::find()->where(['name' => 'Жюри конкурса'])->one();
        $urlToPage = '/' .$_menu->url;
        unset($_menu);

        return $this->render('_main_page_jury', ['juryArr' => $juryArr, 'urlToPage' => $urlToPage]);
    }
}