<?php

$this->title = $menu->seo_title." - ".Yii::$app->params['seo_title'];
Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $menu->seo_keywords]);
Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $menu->seo_description]);
?>
<section class="jury">
    <div class="container">

        <?php
        if (empty($juries)) {
            ?>
<!--        <div class="member">-->
<!---->
<!--        </div>-->
        <?php
        } else {

            foreach ($juries as $_jury) {
                ?>
                <div class="member">
                    <a name="<?=\common\helpers\CString::translitTo($_jury->flp)?>"></a>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <?=\yii\helpers\Html::img("/".$_jury->img, ['alt' => $_jury->flp, 'style' => 'width:100%;'])?>
                        </div>
                        <div class="col-md-9 col-sm-6 col-xs-6">
                            <h2><?=$_jury->flp?></h2>
                            <?=$_jury->bio?>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
        ?>
    </div>
</section>