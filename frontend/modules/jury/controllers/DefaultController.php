<?php
/**
 * Created by PhpStorm.
 * User: pashaevs
 * Date: 03.01.15
 * Time: 18:00
 */

namespace frontend\modules\jury\controllers;

use common\modules\jury\models\Jury;
use common\modules\menu\models\Menu;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex($menu_url)
    {
        $menu = Menu::find()->where('url = :url', [':url' => $menu_url])->one();
        $juries = Jury::find()->orderBy('position ASC')->all();

        return $this->render('index', ['juries' => $juries, 'menu' => $menu]);
    }
}