<?php
/**
 * Created by PhpStorm.
 * User: pashaevs
 * Date: 10.03.15
 * Time: 4:09
 */

namespace frontend\modules\gallery\widgets;

use common\modules\gallery\models\Gallery;
use yii\base\Widget;

class GalleryWidget extends Widget {

    public function run() {

        $main = Gallery::find()->where(['is_main' => 1])->one();

        $galleries = Gallery::find()->orderBy('position ASC')->all();

        return $this->render('_gallery', ['main' => $main, 'galleries' => $galleries]);
    }
}