<?php
use \yii\helpers\Html;
?>
<div class="vid_gallery">
    <div class="player">
        <div class="play"></div>
        <div class="cont">
            <?=Html::img("/".$main->doCache('800x379', 'auto'))?>
        </div>
    </div>
    <div class="list">
        <?php
        foreach ($galleries as $_gal) {
            $video_href = $_gal->videoUrl();
            echo Html::beginTag('div', ['class' => 'item'.(empty($video_href) ? "" : " video")]);
            if ($_gal->video != "") {
                echo Html::img("/".$_gal->doCache('800x379', 'auto'), ['video-href' => $video_href]);
            } else {
                echo Html::img("/".$_gal->doCache('800x379', 'auto'));
            }

            echo Html::endTag('div');
        }
        ?>
    </div>
</div>