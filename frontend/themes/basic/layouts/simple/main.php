<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAssetSimple;
use frontend\widgets\Alert;

/**
 * @var \yii\web\View $this
 * @var string $content
 */

AppAssetSimple::register($this);


$user = \common\modules\user\models\User::findIdentity(Yii::$app->user->getId());
$is_admin = false;
if (!empty($user) && ($user->role->id == 1 || $user->role->id == 2)) {
    $is_admin = true;
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<br/>
<div class="container-fluid" style="margin: 10px 20px 20px 20px;">
    <?= Alert::widget() ?>
    <?= $content ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
