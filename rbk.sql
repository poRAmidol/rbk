-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 20, 2015 at 05:34 PM
-- Server version: 5.5.38
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rbk`
--

-- --------------------------------------------------------

--
-- Table structure for table `frontend_session`
--

CREATE TABLE `frontend_session` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `frontend_session`
--

INSERT INTO `frontend_session` (`id`, `expire`, `data`) VALUES
('75f724f4b278369ffb1da93128dfcc64', 1426859679, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f6d61696e2f64656661756c742f636170746368617c733a363a22373231303233223b5f5f636170746368612f6d61696e2f64656661756c742f63617074636861636f756e747c693a313b),
('d07e52054a77d31e651c919cbb5b258c', 1426859473, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b5f5f636170746368612f6d61696e2f64656661756c742f636170746368617c733a363a22373636373937223b5f5f636170746368612f6d61696e2f64656661756c742f63617074636861636f756e747c693a313b);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_comment`
--

CREATE TABLE `t_kg_comment` (
`id` int(11) NOT NULL,
  `email` varchar(512) NOT NULL COMMENT 'Имя пользователя',
  `date` int(11) NOT NULL COMMENT 'Дата',
  `text` text NOT NULL COMMENT 'Текст',
  `user_id` int(11) DEFAULT NULL COMMENT 'Пользователь',
  `parent_id` int(11) DEFAULT NULL COMMENT 'Владелец',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT 'Уровень',
  `model_name` varchar(512) NOT NULL COMMENT 'Название модели',
  `model_id` int(11) NOT NULL COMMENT 'Номер модели',
  `publish` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Публиковать'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Комментарии' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `t_kg_comment`
--

INSERT INTO `t_kg_comment` (`id`, `email`, `date`, `text`, `user_id`, `parent_id`, `level`, `model_name`, `model_id`, `publish`) VALUES
(3, 'abdulaev.rashid@gmail.com', 1412625600, 'dfsdf asdfasdf asdfasdfasd fa sdf asdf asdf asdf asdf asdf asdfas dfa sdf asdf asdf asdf asdf ', NULL, NULL, 0, 'common\\modules\\cases\\models\\Cases', 65, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_competition_stage`
--

CREATE TABLE `t_kg_competition_stage` (
`id` int(11) NOT NULL,
  `number` int(11) NOT NULL COMMENT 'Номер этапа',
  `title` varchar(512) NOT NULL COMMENT 'Название этапа',
  `url` varchar(512) NOT NULL COMMENT 'Ссылка на страницу этапа',
  `note` varchar(512) NOT NULL COMMENT 'Примечание',
  `date` varchar(254) NOT NULL COMMENT 'Дата проведения этапа',
  `current_stage` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Отметить как текущий этап',
  `past_stage` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Отметить как пройденый этап'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица Этапов конкурса' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `t_kg_competition_stage`
--

INSERT INTO `t_kg_competition_stage` (`id`, `number`, `title`, `url`, `note`, `date`, `current_stage`, `past_stage`) VALUES
(1, 1, 'Прием заявок соискателей', '', '', 'C 23 ноября 2014 по 15 марта 2015', 1, 0),
(2, 2, 'Проведение обучающих семинаров', '', 'Для участия необходима регистрация', ' С 26 января по 22 февраля 2015', 0, 0),
(3, 3, 'Подготовка лонг- листа, сбор креативных материалов', '', '', 'C 23 ноября 2014 по 15 марта 2015', 0, 0),
(4, 4, 'Заседание жюри первого этапа: составление шорт-листа', '', '', 'С 16 до 22 марта 2015', 0, 0),
(5, 5, 'Заседание жюри второго этапа: выбор победителей', '', '', 'С 23 по 31 матра 2015', 0, 0),
(6, 6, 'Конференция в рамках премии', '', 'Для участия необходима регистрация', '2 апреля 2015 ', 0, 0),
(7, 7, 'Церемония вручения премии', '', '', '2 апреля 2014', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_content_block`
--

CREATE TABLE `t_kg_content_block` (
`id` int(11) NOT NULL,
  `name` varchar(254) NOT NULL COMMENT 'Название блока',
  `content` mediumtext NOT NULL COMMENT 'Содержимое',
  `visible` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица статической информации' AUTO_INCREMENT=13 ;

--
-- Dumping data for table `t_kg_content_block`
--

INSERT INTO `t_kg_content_block` (`id`, `name`, `content`, `visible`) VALUES
(1, 'Главня страница - приветствие', '<p style="font-size:16px; line-height:24px;">\r\n	<strong>EFFIE RUSSIA&nbsp;&mdash; БРЭНД ГОДА</strong>&nbsp;&mdash; национальный конкурс самой престижной в&nbsp;мире награды за&nbsp;достижения в&nbsp;маркетинге EFFIE AWARDS.\r\n</p>', 1),
(3, 'О премии - Текст на главной странице сайта', '<a href="/about-award.html" class="h1">О премии</a>\r\n                <p>\r\n                    Награда Effie вручается за главное достижение в сфере рекламы и маркетинговых коммуникации — эффективность. Организованный в 1968 году Нью-Йоркской Маркетинговой Ассоциацией, конкурс Effie является единственной национальной наградой способной оценить успехи компании во взаимодействии с целевой аудиторией, а также достигнутые результаты. Помимо национальных программ Effie существует конкурс Effie Европа и глобальный  Effie.\r\n                </p>\r\n                <p>\r\n                    Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России, Брэнд года стал наиболее значимой наградой для компаний, нацеленных на лидерство на рынке  независимо от отрасли бизнеса. С 2001 года российская национальная награда в области построения брэндов Брэнд года входит в международную систему Effie — самую авторитетную мировую награду в сфере эффективных маркетинговых коммуникаций проводимую в 33-х странах мира. \r\n                </p>', 1),
(4, 'Открыт прием заявок - Главная страница сайта', '<h1>Открыт прием заявок</h1>\r\n<p>\r\n	Effie/Брэнд года награждает самые эффективные идеи и стратегии маркетинговых коммуникаций, которые были реализованы в 2014 году, и привели к&nbsp;достижению или перевыполнению поставленных целей.\r\n</p>\r\n<p>\r\n	В конкурсе Effie Russia может принять участие любая компания, которая в реализовала проекты или проекты маркетинговых коммуникаций на территории России с 1 января по 31 декабря 2014 года.\r\n</p>\r\n<p>\r\n	Стоимость одной заявки (60,000 \r\n	<span class="hyphen"></span><span class="ruble">p</span><span class="dot">уб.</span> + НДС) включает обработку заявки и организацию процедуры оценки всех участников на равной и объективной основе, в соответствии с критериями и правилами, установленными Effie Global.\r\n</p>\r\n<h3>Семинар для соискателей премии</h3>\r\n<p>\r\n	Посетите обучающий семинар, на котором оргкомитет Effie поможет корректно заполнить заявку на участие. Семинар состоится в Пресс-центре РБК, стоимость участия 10,000 \r\n	<span class="hyphen"></span><span class="ruble">p</span><span class="dot">уб.</span> + НДС\r\n</p>', 1),
(5, 'Номинации - Главная страница сайта', '<div class="container-text">\r\n<p>\r\n	<a href="/nomination.html" class="h1">Номинации</a>\r\n</p>\r\n<h3>Основной конкурс</h3>\r\n<ol>\r\n	<li>Мультибрендовые и мультикатегорийные компании</li>\r\n	<li>Банки, страховые компании, строительство телекоммуникаций, интернет и другие услуги связи</li>\r\n	<li>Товары для дома</li>\r\n	<li>Продукты питания</li>\r\n	<li>Одежда, обувь, украшения</li>\r\n	<li>Отдых, развлечения, туризм, культура, спорт, образование</li>\r\n	<li>Услуги для бизнеса</li>\r\n	<li>Здоровье, средства по уходу, товары гигиены</li>\r\n</ol>\r\n<p>\r\n	<br>\r\n</p>\r\n<p>\r\n	<br>\r\n</p>\r\n<p>\r\n	<br>\r\n</p>\r\n<ol>\r\n</ol>\r\n<ol>\r\n</ol>\r\n<h3>Специальные номинации</h3>\r\n<ol>\r\n	<li>Успешный ребрендинг</li>\r\n	<li>Социальный бренд / КСО</li>\r\n	<li>Инновационный бренд</li>\r\n	<li>Корпоративнй бренд</li>\r\n	<li>Бренд без бюджета</li>\r\n</ol>\r\n</div>', 1),
(6, 'Партнеры - Главная страница сайта', '<a href="/partners.html" class="h1">Партнеры</a><h3>Генеральные партнеры премии</h3><div class="row">\r\n	<div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#mersedez-benz"><img src="/img/photo/partners/1.jpg" alt=""></a>\r\n	</div>\r\n	<div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#gazprom"><img src="/img/photo/partners/2.jpg" alt=""></a>\r\n	</div>\r\n	<div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#dozjdlive"><img src="/img/photo/partners/3.jpg" alt=""></a>\r\n	</div>\r\n	<div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#sberbank"><img src="/img/photo/partners/4.jpg" alt=""></a>\r\n	</div>\r\n	<div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#vtb24"><img src="/img/photo/partners/5.jpg" alt=""></a>\r\n	</div>\r\n	<div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#rosgosstrah"><img src="/img/photo/partners/6.jpg" alt=""></a>\r\n	</div>\r\n	<div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#mersedez-benz"><img src="/img/photo/partners/1.jpg" alt=""></a>\r\n	</div>\r\n	<div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#gazprom"><img src="/img/photo/partners/2.jpg" alt=""></a>\r\n	</div>\r\n</div><h3>Информационные партнеры премии</h3><div class="row"><div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#mersedez-benz"><img src="/img/photo/partners/1.jpg" alt=""></a>\r\n	</div><div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#mersedez-benz"><img src="/img/photo/partners/2.jpg" alt=""></a>\r\n	</div><div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#mersedez-benz"><img src="/img/photo/partners/3.jpg" alt=""></a>\r\n	</div><div class="col-xs-6 col-sm-3 col-md-3">\r\n		<a href="/partners.html#mersedez-benz"><img src="/img/photo/partners/4.jpg" alt=""></a>\r\n	</div></div>', 1),
(7, 'Контакты Организаторов премии - Главная страница', '<div class="row">\r\n<h1>Контакты</h1>\r\n<div class="col-xs-12 col-sm-12 col-md-3 contact-img-rbk">\r\n	<img src="/uploads/1/rbkbig.png">\r\n	<h3>Организатор премии</h3>\r\n</div>\r\n<div class="col-xs-12 col-sm-12 col-md-9">\r\n<div class="row">\r\n<div class="col-xs-6 col-sm-6 col-md-4 contact-item">\r\n\r\n		<h3>Руководитель проекта</h3>\r\n		<span class="">Владислав Кучмистый</span>\r\n		<span class="mail">vkuchmistyy@rbc.ru</span>\r\n		<span class="">+7 495 363 11 11 доб. 2272</span>\r\n</div>\r\n			<div class="col-xs-6 col-sm-6 col-md-4 contact-item">\r\n	<h3>Вопросы участия</h3>\r\n	<span class="">Алина Горбашова</span>\r\n	<span class="mail">a.gorbashova@rbc.ru</span><span class="">+7 495 363 11 11 доб. 1610</span>\r\n</div>\r\n			<div class="col-xs-6 col-sm-6 col-md-4 contact-item">\r\n	<h3>Реклама и спонсорство</h3>\r\n	<span class="">Ольга Макарова</span>\r\n	<span class="mail">omakarova@rbc.ru</span>\r\n	<span class="">+7 495 363 11 11 доб. 1358</span>\r\n</div>\r\n			<div class="col-xs-6 col-sm-6 col-md-4 contact-item">\r\n	<h3>PR</h3>\r\n	<span class="">Анна Фёдорова</span>\r\n	<span class="mail">a.fedorova@rbc.ru</span>\r\n	<span class="">+ 7 495 363 11 11 доб. 1359</span>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 1),
(8, 'Положение о премии - Страница О премии', '<p style="line-height:40px;">\r\n	<a href="/uploads/14/effieaward2014.zip"></a>\r\n</p>\r\n<p style="line-height:40px;">\r\n	<a target="_blank" href="/uploads/13/Effie_Awards_RBC_2014_16.pptx">Скачать презентацию премии</a>\r\n</p>\r\n<p style="line-height:40px;">\r\n	<a href="/uploads/13/Position_EFFIE_RUSSIA.docx">Скачать положение о премии</a>\r\n</p>\r\n<p style="line-height:40px;">\r\n	<a href="/uploads/14/effie_award_2014.zip">Скачать бланки заявки</a>\r\n</p>\r\n<h2><a class="last-winners" href="/past-winners.html">Победители прошлых лет</a></h2>', 1),
(9, 'Партнеры - Страница партнеров', '<section class="partners">\r\n<div class="container">\r\n	<div class="row">\r\n		<h1>Генеральные партнеры премии</h1>\r\n		<div class="col-md-6"><a name="mersedez-benz"></a>\r\n			<div class="logo">\r\n				<img src="/uploads/1/1.jpg">\r\n			</div>\r\n			<h2>Mersedez-benz</h2>\r\n			<p>\r\n				МБ-Беляево, официальный дилер «Мерседес - Бенц» в Москве,\r\nуспешно работает на российском рынке с 1992 года.\r\nОсновополагающими принципами, которые обеспечили\r\nдилерскому центру репутаци солидного партнера и доверие\r\nклиентов, являются высокий профессионализм сотрудников\r\nкомпании и высокое качество предлагаемых услуг.\r\n			</p>\r\n		</div>\r\n		<div class="col-md-6"><a name="gazprom"></a>\r\n			<div class="logo">\r\n				<img src="/uploads/1/2.jpg">\r\n			</div>\r\n			<h2>ОАО Газпром</h2>\r\n			<p>\r\n				ОАО «Газпром» — глобальная энергетическая компания.\r\nОсновные направления деятельности — геологоразведка,\r\nдобыча, транспортировка, хранение, переработка\r\nи реализация газа, газового конденсата и нефти, реализация\r\nгаза в качестве моторного топлива, а также производство\r\nи сбыт тепло- и электроэнергии.\r\n			</p>\r\n		</div>\r\n		<div class="col-md-6">\r\n			<a name="dozjdlive"></a>\r\n			<div class="logo">\r\n				<img src="/uploads/1/3.jpg">\r\n			</div>\r\n			<h2>Телеканал "Дождь"</h2>\r\n			<p>\r\n				МБ-Беляево, официальный дилер «Мерседес - Бенц» в Москве,\r\nуспешно работает на российском рынке с 1992 года.\r\nОсновополагающими принципами, которые обеспечили\r\nдилерскому центру репутаци солидного партнера и доверие\r\nклиентов, являются высокий профессионализм сотрудников\r\nкомпании и высокое качество предлагаемых услуг.\r\n			</p>\r\n		</div>\r\n		<div class="col-md-6"><a name="sberbank"></a>\r\n			<div class="logo">\r\n				<img src="/uploads/1/4.jpg">\r\n			</div>\r\n			<h2>ОАО "Сбербанк"</h2>\r\n			<p>\r\n				ОАО «Газпром» — глобальная энергетическая компания.\r\nОсновные направления деятельности — геологоразведка,\r\nдобыча, транспортировка, хранение, переработка\r\nи реализация газа, газового конденсата и нефти, реализация\r\nгаза в качестве моторного топлива, а также производство\r\nи сбыт тепло- и электроэнергии.\r\n			</p>\r\n		</div>\r\n		<h1>Информационные партнеры премии</h1>\r\n		<div class="col-md-6"><a name="vtb24"></a>\r\n			<div class="logo">\r\n				<img src="/uploads/1/5.jpg">\r\n			</div>\r\n			<h2>Банк ВТБ 24</h2>\r\n			<p>\r\n				МБ-Беляево, официальный дилер «Мерседес - Бенц» в Москве,\r\nуспешно работает на российском рынке с 1992 года.\r\nОсновополагающими принципами, которые обеспечили\r\nдилерскому центру репутаци солидного партнера и доверие\r\nклиентов, являются высокий профессионализм сотрудников\r\nкомпании и высокое качество предлагаемых услуг.\r\n			</p>\r\n		</div>\r\n		<div class="col-md-6">\r\n			<div class="logo"><a name="rosgosstrah"></a>\r\n				<img src="/uploads/1/6.jpg">\r\n			</div>\r\n			<h2>ОАО Россгосстрах</h2>\r\n			<p>\r\n				ОАО «Газпром» — глобальная энергетическая компания.\r\nОсновные направления деятельности — геологоразведка,\r\nдобыча, транспортировка, хранение, переработка\r\nи реализация газа, газового конденсата и нефти, реализация\r\nгаза в качестве моторного топлива, а также производство\r\nи сбыт тепло- и электроэнергии.\r\n			</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n</section>', 1),
(10, 'Номинации', '<h1>Номинации 2014</h1>\r\n<p>\r\n	Конкурс Effie Russia отмечает эффективные кампании во всем спектре товаров и услуг — от товаров массового спроса до финансовых услуг и имиджа корпораций. Брэнды участвуют и побеждают в 17 основных категориях конкурса и 4 специальных номинациях. Проекты, реализованные на российском рынке, оцениваются с точки зрения требований, предъявляемых к эффективным маркетинговым стратегиям во всем мире.\r\n</p>', 1),
(11, 'Комментарий к кнопке Регистрации', '<p>\r\n	Для подачи заявки на&nbsp;конкурс и&nbsp;участия в&nbsp;семинаре пройдите онлайн-регистрацию. Бланки заявки мы&nbsp;пришлем по&nbsp;электронной почте.\r\n</p>', 1),
(12, 'О премии', '<h1>О премии</h1><p>\r\n	<a href="asdasdasD">Награда</a> Effie вручается за главное достижение в сфере рекламы и маркетинговых коммуникаций – эффективность. Организованный в 1968 году Нью-Йоркской Маркетинговой <a href="/uploads/11/julijasoloveva.jpg">Ассоциацией</a>, конкурс Effie является единственной национальной наградой, способной оценить успехи компании во взаимодействии с целевой аудиторией, а также достигнутые результаты. Помимо национальных программ Effie существует конкурс Effie Europe и Effie Global.\r\n</p><p>\r\n	<a href="/cp/uploads/11/ocenka-raboty-po-razrabotke-sajta-dla-gorodstkoj-bolnicy.docx"></a><a href="/uploads/11/snimok-ekrana-2015-02-12-v-131041.png">Миссия Effie</a> – вдохновлять компании на эффективные маркетинговые решения, популяризировать маркетинговые коммуникации, как инструмент продвижения бренда и развития бизнеса.\r\n</p><p>\r\n	<a href="/uploads/11/vasillacanich.jpg">Идеология Effie </a>– награждать самые эффективные идеи и стратегии маркетинговых коммуникаций, которые были реализованы в 2014 году, и привели к достижению или перевыполнению поставленных целей.\r\n</p><p>\r\n	<a href="/cp/uploads/11/julijasoloveva.jpg">Слоган Effie – награждать идеи, которые работают.</a>\r\n</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_draft`
--

CREATE TABLE `t_kg_draft` (
`id` int(11) NOT NULL,
  `model_name` varchar(512) NOT NULL COMMENT 'Название модели',
  `model_id` int(11) NOT NULL COMMENT 'Номер модели',
  `model_obj` longtext NOT NULL COMMENT 'Сериализованная модель'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица где храняться черновики' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `t_kg_draft`
--

INSERT INTO `t_kg_draft` (`id`, `model_name`, `model_id`, `model_obj`) VALUES
(1, 'common\\modules\\jury\\models\\Jury', 6, 'O:31:"common\\modules\\jury\\models\\Jury":8:{s:36:"\0yii\\db\\BaseActiveRecord\0_attributes";a:4:{s:3:"flp";s:25:"Роман Баданин";s:3:"bio";s:477:"Руководит сайтом Rbc.Ru с января 2014 года. В течение 10 лет был заместителем главного редактора сайта Gazeta. Ru. С декабря 2011 года Роман Баданин стал шеф-редактором сайта Forbes.Ru. С осени 2013 года – исполнительным директором службы интернет-проектов агентства «Интерфакс».";s:3:"img";s:37:"images/modules/jury/roman_badanin.jpg";s:2:"id";i:6;}s:39:"\0yii\\db\\BaseActiveRecord\0_oldAttributes";a:4:{s:3:"flp";s:25:"Роман Баданин";s:3:"bio";s:477:"Руководит сайтом Rbc.Ru с января 2014 года. В течение 10 лет был заместителем главного редактора сайта Gazeta. Ru. С декабря 2011 года Роман Баданин стал шеф-редактором сайта Forbes.Ru. С осени 2013 года – исполнительным директором службы интернет-проектов агентства «Интерфакс».";s:3:"img";s:37:"images/modules/jury/roman_badanin.jpg";s:2:"id";i:6;}s:33:"\0yii\\db\\BaseActiveRecord\0_related";a:0:{}s:23:"\0yii\\base\\Model\0_errors";a:0:{}s:27:"\0yii\\base\\Model\0_validators";C:11:"ArrayObject":2401:{x:i:0;a:4:{i:0;O:32:"yii\\validators\\RequiredValidator":14:{s:11:"skipOnEmpty";b:0;s:13:"requiredValue";N;s:6:"strict";b:0;s:7:"message";s:56:"Необходимо заполнить «{attribute}».";s:10:"attributes";a:2:{i:0;s:3:"flp";i:1;s:3:"bio";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:1;O:30:"yii\\validators\\StringValidator":19:{s:6:"length";N;s:3:"max";N;s:3:"min";N;s:7:"message";s:70:"Значение «{attribute}» должно быть строкой.";s:8:"tooShort";N;s:7:"tooLong";N;s:8:"notEqual";N;s:8:"encoding";s:5:"UTF-8";s:10:"attributes";a:1:{i:0;s:3:"bio";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:2;O:30:"yii\\validators\\NumberValidator":19:{s:11:"integerOnly";b:1;s:3:"max";N;s:3:"min";N;s:6:"tooBig";N;s:8:"tooSmall";N;s:14:"integerPattern";s:18:"/^\\s*[+-]?\\d+\\s*$/";s:13:"numberPattern";s:48:"/^\\s*[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?\\s*$/";s:10:"attributes";a:1:{i:0;s:7:"publish";}s:7:"message";s:79:"Значение «{attribute}» должно быть целым числом.";s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:3;O:30:"yii\\validators\\StringValidator":19:{s:6:"length";N;s:3:"max";i:512;s:3:"min";N;s:7:"message";s:70:"Значение «{attribute}» должно быть строкой.";s:8:"tooShort";N;s:7:"tooLong";s:194:"Значение «{attribute}» должно содержать максимум {max, number} {max, plural, one{символ} few{символа} many{символов} other{символа}}.";s:8:"notEqual";N;s:8:"encoding";s:5:"UTF-8";s:10:"attributes";a:1:{i:0;s:3:"flp";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}};m:a:0:{}}s:25:"\0yii\\base\\Model\0_scenario";s:7:"default";s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";a:0:{}}'),
(2, 'common\\modules\\jury\\models\\Jury', 10, 'O:31:"common\\modules\\jury\\models\\Jury":8:{s:36:"\0yii\\db\\BaseActiveRecord\0_attributes";a:5:{s:3:"img";s:0:"";s:3:"flp";s:15:"sdasad asd asD ";s:8:"position";i:99;s:3:"bio";s:31:"<p> asdasD asd asD asd asD </p>";s:2:"id";i:10;}s:39:"\0yii\\db\\BaseActiveRecord\0_oldAttributes";a:5:{s:3:"img";s:0:"";s:3:"flp";s:15:"sdasad asd asD ";s:8:"position";i:99;s:3:"bio";s:31:"<p> asdasD asd asD asd asD </p>";s:2:"id";i:10;}s:33:"\0yii\\db\\BaseActiveRecord\0_related";a:0:{}s:23:"\0yii\\base\\Model\0_errors";a:0:{}s:27:"\0yii\\base\\Model\0_validators";C:11:"ArrayObject":5852:{x:i:0;a:7:{i:0;O:32:"yii\\validators\\RequiredValidator":14:{s:11:"skipOnEmpty";b:0;s:13:"requiredValue";N;s:6:"strict";b:0;s:7:"message";s:56:"Необходимо заполнить «{attribute}».";s:10:"attributes";a:2:{i:0;s:3:"flp";i:1;s:3:"bio";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:1;O:30:"yii\\validators\\StringValidator":19:{s:6:"length";N;s:3:"max";N;s:3:"min";N;s:7:"message";s:70:"Значение «{attribute}» должно быть строкой.";s:8:"tooShort";N;s:7:"tooLong";N;s:8:"notEqual";N;s:8:"encoding";s:5:"UTF-8";s:10:"attributes";a:1:{i:0;s:3:"bio";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:2;O:30:"yii\\validators\\NumberValidator":19:{s:11:"integerOnly";b:1;s:3:"max";N;s:3:"min";N;s:6:"tooBig";N;s:8:"tooSmall";N;s:14:"integerPattern";s:18:"/^\\s*[+-]?\\d+\\s*$/";s:13:"numberPattern";s:48:"/^\\s*[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?\\s*$/";s:10:"attributes";a:2:{i:0;s:7:"publish";i:1;s:8:"position";}s:7:"message";s:79:"Значение «{attribute}» должно быть целым числом.";s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:3;O:36:"yii\\validators\\DefaultValueValidator":13:{s:5:"value";i:99;s:11:"skipOnEmpty";b:0;s:10:"attributes";a:1:{i:0;s:8:"position";}s:7:"message";N;s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:4;O:29:"yii\\validators\\ImageValidator":33:{s:8:"notImage";s:67:"Файл «{file}» не является изображением.";s:8:"minWidth";N;s:8:"maxWidth";N;s:9:"minHeight";N;s:9:"maxHeight";N;s:10:"underWidth";s:219:"Файл «{file}» слишком маленький. Ширина должна быть более {limit, number} {limit, plural, one{пиксель} few{пикселя} many{пикселей} other{пикселя}}.";s:9:"overWidth";s:219:"Файл «{file}» слишком большой. Ширина не должна превышать {limit, number} {limit, plural, one{пиксель} few{пикселя} many{пикселей} other{пикселя}}.";s:11:"underHeight";s:219:"Файл «{file}» слишком маленький. Высота должна быть более {limit, number} {limit, plural, one{пиксель} few{пикселя} many{пикселей} other{пикселя}}.";s:10:"overHeight";s:219:"Файл «{file}» слишком большой. Высота не должна превышать {limit, number} {limit, plural, one{пиксель} few{пикселя} many{пикселей} other{пикселя}}.";s:10:"extensions";a:4:{i:0;s:3:"jpg";i:1;s:4:"jpeg";i:2;s:3:"gif";i:3;s:3:"png";}s:24:"checkExtensionByMimeType";b:1;s:9:"mimeTypes";a:0:{}s:7:"minSize";N;s:7:"maxSize";N;s:8:"maxFiles";i:1;s:7:"message";s:48:"Загрузка файла не удалась.";s:14:"uploadRequired";s:28:"Загрузите файл.";s:6:"tooBig";s:197:"Файл «{file}» слишком большой. Размер не должен превышать {limit, number} {limit, plural, one{байт} few{байта} many{байт} other{байта}}.";s:8:"tooSmall";s:197:"Файл «{file}» слишком маленький. Размер должен быть более {limit, number} {limit, plural, one{байт} few{байта} many{байт} other{байта}}.";s:7:"tooMany";s:157:"Вы не можете загружать более {limit, number} {limit, plural, one{файла} few{файлов} many{файлов} other{файла}}.";s:14:"wrongExtension";s:127:"Разрешена загрузка файлов только со следующими расширениями: {extensions}.";s:13:"wrongMimeType";s:119:"Разрешена загрузка файлов только со следующими MIME-типами: {mimeTypes}.";s:10:"attributes";a:1:{i:0;s:3:"img";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:5;O:28:"yii\\validators\\SafeValidator":12:{s:10:"attributes";a:1:{i:0;s:3:"img";}s:7:"message";N;s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:6;O:30:"yii\\validators\\StringValidator":19:{s:6:"length";N;s:3:"max";i:512;s:3:"min";N;s:7:"message";s:70:"Значение «{attribute}» должно быть строкой.";s:8:"tooShort";N;s:7:"tooLong";s:194:"Значение «{attribute}» должно содержать максимум {max, number} {max, plural, one{символ} few{символа} many{символов} other{символа}}.";s:8:"notEqual";N;s:8:"encoding";s:5:"UTF-8";s:10:"attributes";a:1:{i:0;s:3:"flp";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}};m:a:0:{}}s:25:"\0yii\\base\\Model\0_scenario";s:7:"default";s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";a:0:{}}'),
(3, 'common\\modules\\jury\\models\\Jury', 11, 'O:31:"common\\modules\\jury\\models\\Jury":8:{s:36:"\0yii\\db\\BaseActiveRecord\0_attributes";a:5:{s:3:"img";s:0:"";s:3:"flp";s:15:"sfa sdf asdf as";s:8:"position";i:99;s:3:"bio";s:27:"<p>a sdfasdf asdf asd f</p>";s:2:"id";i:11;}s:39:"\0yii\\db\\BaseActiveRecord\0_oldAttributes";a:5:{s:3:"img";s:0:"";s:3:"flp";s:15:"sfa sdf asdf as";s:8:"position";i:99;s:3:"bio";s:27:"<p>a sdfasdf asdf asd f</p>";s:2:"id";i:11;}s:33:"\0yii\\db\\BaseActiveRecord\0_related";a:0:{}s:23:"\0yii\\base\\Model\0_errors";a:0:{}s:27:"\0yii\\base\\Model\0_validators";C:11:"ArrayObject":5852:{x:i:0;a:7:{i:0;O:32:"yii\\validators\\RequiredValidator":14:{s:11:"skipOnEmpty";b:0;s:13:"requiredValue";N;s:6:"strict";b:0;s:7:"message";s:56:"Необходимо заполнить «{attribute}».";s:10:"attributes";a:2:{i:0;s:3:"flp";i:1;s:3:"bio";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:1;O:30:"yii\\validators\\StringValidator":19:{s:6:"length";N;s:3:"max";N;s:3:"min";N;s:7:"message";s:70:"Значение «{attribute}» должно быть строкой.";s:8:"tooShort";N;s:7:"tooLong";N;s:8:"notEqual";N;s:8:"encoding";s:5:"UTF-8";s:10:"attributes";a:1:{i:0;s:3:"bio";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:2;O:30:"yii\\validators\\NumberValidator":19:{s:11:"integerOnly";b:1;s:3:"max";N;s:3:"min";N;s:6:"tooBig";N;s:8:"tooSmall";N;s:14:"integerPattern";s:18:"/^\\s*[+-]?\\d+\\s*$/";s:13:"numberPattern";s:48:"/^\\s*[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?\\s*$/";s:10:"attributes";a:2:{i:0;s:7:"publish";i:1;s:8:"position";}s:7:"message";s:79:"Значение «{attribute}» должно быть целым числом.";s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:3;O:36:"yii\\validators\\DefaultValueValidator":13:{s:5:"value";i:99;s:11:"skipOnEmpty";b:0;s:10:"attributes";a:1:{i:0;s:8:"position";}s:7:"message";N;s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:4;O:29:"yii\\validators\\ImageValidator":33:{s:8:"notImage";s:67:"Файл «{file}» не является изображением.";s:8:"minWidth";N;s:8:"maxWidth";N;s:9:"minHeight";N;s:9:"maxHeight";N;s:10:"underWidth";s:219:"Файл «{file}» слишком маленький. Ширина должна быть более {limit, number} {limit, plural, one{пиксель} few{пикселя} many{пикселей} other{пикселя}}.";s:9:"overWidth";s:219:"Файл «{file}» слишком большой. Ширина не должна превышать {limit, number} {limit, plural, one{пиксель} few{пикселя} many{пикселей} other{пикселя}}.";s:11:"underHeight";s:219:"Файл «{file}» слишком маленький. Высота должна быть более {limit, number} {limit, plural, one{пиксель} few{пикселя} many{пикселей} other{пикселя}}.";s:10:"overHeight";s:219:"Файл «{file}» слишком большой. Высота не должна превышать {limit, number} {limit, plural, one{пиксель} few{пикселя} many{пикселей} other{пикселя}}.";s:10:"extensions";a:4:{i:0;s:3:"jpg";i:1;s:4:"jpeg";i:2;s:3:"gif";i:3;s:3:"png";}s:24:"checkExtensionByMimeType";b:1;s:9:"mimeTypes";a:0:{}s:7:"minSize";N;s:7:"maxSize";N;s:8:"maxFiles";i:1;s:7:"message";s:48:"Загрузка файла не удалась.";s:14:"uploadRequired";s:28:"Загрузите файл.";s:6:"tooBig";s:197:"Файл «{file}» слишком большой. Размер не должен превышать {limit, number} {limit, plural, one{байт} few{байта} many{байт} other{байта}}.";s:8:"tooSmall";s:197:"Файл «{file}» слишком маленький. Размер должен быть более {limit, number} {limit, plural, one{байт} few{байта} many{байт} other{байта}}.";s:7:"tooMany";s:157:"Вы не можете загружать более {limit, number} {limit, plural, one{файла} few{файлов} many{файлов} other{файла}}.";s:14:"wrongExtension";s:127:"Разрешена загрузка файлов только со следующими расширениями: {extensions}.";s:13:"wrongMimeType";s:119:"Разрешена загрузка файлов только со следующими MIME-типами: {mimeTypes}.";s:10:"attributes";a:1:{i:0;s:3:"img";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:5;O:28:"yii\\validators\\SafeValidator":12:{s:10:"attributes";a:1:{i:0;s:3:"img";}s:7:"message";N;s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}i:6;O:30:"yii\\validators\\StringValidator":19:{s:6:"length";N;s:3:"max";i:512;s:3:"min";N;s:7:"message";s:70:"Значение «{attribute}» должно быть строкой.";s:8:"tooShort";N;s:7:"tooLong";s:194:"Значение «{attribute}» должно содержать максимум {max, number} {max, plural, one{символ} few{символа} many{символов} other{символа}}.";s:8:"notEqual";N;s:8:"encoding";s:5:"UTF-8";s:10:"attributes";a:1:{i:0;s:3:"flp";}s:2:"on";a:0:{}s:6:"except";a:0:{}s:11:"skipOnError";b:1;s:11:"skipOnEmpty";b:1;s:22:"enableClientValidation";b:1;s:7:"isEmpty";N;s:4:"when";N;s:10:"whenClient";N;s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";N;}};m:a:0:{}}s:25:"\0yii\\base\\Model\0_scenario";s:7:"default";s:27:"\0yii\\base\\Component\0_events";a:0:{}s:30:"\0yii\\base\\Component\0_behaviors";a:0:{}}');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_file`
--

CREATE TABLE `t_kg_file` (
`id` int(11) NOT NULL,
  `model_name` varchar(512) NOT NULL COMMENT 'Название модели',
  `model_id` int(11) NOT NULL COMMENT 'Номер модели',
  `name` varchar(512) NOT NULL COMMENT 'Имя файлв',
  `src` varchar(512) NOT NULL COMMENT 'Путь к файлу',
  `mime_type` varchar(64) NOT NULL COMMENT 'Тип файла'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Файлы' AUTO_INCREMENT=28 ;

--
-- Dumping data for table `t_kg_file`
--

INSERT INTO `t_kg_file` (`id`, `model_name`, `model_id`, `name`, `src`, `mime_type`) VALUES
(2, 'common\\modules\\cases\\models\\Cases', 40, '5f1da9912f661e3071db03ae377ed3ae.jpg', 'images/case/2014/10/05/40', 'image/jpeg'),
(26, 'common\\modules\\cases\\models\\Cases', 65, '3270a81cc78005ef27947af3704dac5d.jpg', 'images/case/2014/10/05/65', 'image/jpeg'),
(27, 'common\\modules\\cases\\models\\Cases', 65, '3998a0ec90023070d9c61916f49a3236.jpg', 'images/case/2014/10/05/65', 'image/jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_gallery`
--

CREATE TABLE `t_kg_gallery` (
`id` int(11) NOT NULL,
  `img_src` varchar(512) NOT NULL COMMENT 'Путь к файлу',
  `img` varchar(512) NOT NULL COMMENT 'Фото',
  `video` text NOT NULL COMMENT 'Ссылка на видео',
  `position` int(11) NOT NULL DEFAULT '99' COMMENT 'Позиция отображения',
  `is_main` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Главная'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Галерея' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `t_kg_gallery`
--

INSERT INTO `t_kg_gallery` (`id`, `img_src`, `img`, `video`, `position`, `is_main`) VALUES
(1, 'images/modules/gallery/1', 'big_img_jpg.jpg', 'http://pics.smotri.com/player.swf?file=v287670703fb&bufferTime=3&autoStart=false&str_lang=rus&xmlsource=http%3A%2F%2Fpics.smotri.com%2Fcskins%2Fblue%2Fskin_color.xml&xmldatasource=http%3A%2F%2Fpics.smotri.com%2Fskin_ng.xml', 1, 1),
(2, 'images/modules/gallery/2', 'photo_1_jpg.jpg', '', 2, 0),
(3, 'images/modules/gallery/3', 'photo_2_jpg.jpg', 'http://www.youtube.com/watch?v=IDbj3rVDMOc', 3, 0),
(4, 'images/modules/gallery/4', 'photo_4_jpg.jpg', '', 99, 0),
(5, 'images/modules/gallery/5', 'photo_5_jpg.jpg', '', 99, 0),
(6, 'images/modules/gallery/6', 'photo_6_jpg.jpg', 'http://www.youtube.com/watch?v=jbUohmP6yoE', 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_jury`
--

CREATE TABLE `t_kg_jury` (
`id` int(11) NOT NULL,
  `img` varchar(512) NOT NULL COMMENT 'Фото',
  `flp` varchar(512) NOT NULL COMMENT 'ФИО',
  `bio` text NOT NULL COMMENT 'Биография',
  `publish` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Опубликовать',
  `position` int(11) NOT NULL DEFAULT '99' COMMENT 'Позиция отображения'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица Жюри' AUTO_INCREMENT=12 ;

--
-- Dumping data for table `t_kg_jury`
--

INSERT INTO `t_kg_jury` (`id`, `img`, `flp`, `bio`, `publish`, `position`) VALUES
(11, '', 'sfa sdf asdf as', '<p>a sdfasdf asdf asd f</p>', 0, 99);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_mail_template`
--

CREATE TABLE `t_kg_mail_template` (
`id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL COMMENT 'Название шаблона',
  `subject` varchar(512) NOT NULL COMMENT 'Заголовок',
  `mail_body` mediumtext NOT NULL COMMENT 'Тело письма'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Шаблоны писем' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `t_kg_mail_template`
--

INSERT INTO `t_kg_mail_template` (`id`, `name`, `subject`, `mail_body`) VALUES
(1, 'Первый шаблон письма', 'Регистрация на конкурс приостановлена', '<p>\r\nПервый шаблон письма\r\n</p>{brand_name}\r\n<p>\r\n	{sdfsdf sdf sdf } sdfsf sdfsdf\r\n</p><p>asdf asdfasdf asdf</p>');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_menu`
--

CREATE TABLE `t_kg_menu` (
`id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'Владелец',
  `name` varchar(512) NOT NULL COMMENT 'Название',
  `alt_name` varchar(512) NOT NULL COMMENT 'Альтернативное название',
  `url` varchar(512) NOT NULL,
  `seo_title` varchar(512) NOT NULL COMMENT 'SEO Заголовок',
  `seo_keywords` varchar(512) NOT NULL COMMENT 'SEO Ключевые слова',
  `seo_description` text NOT NULL COMMENT 'SEO Описание',
  `group_id` int(11) NOT NULL COMMENT 'Название группы меню',
  `position` int(11) DEFAULT '999' COMMENT 'Позиция отображения меню',
  `level` int(11) NOT NULL DEFAULT '0',
  `module_id` int(11) DEFAULT NULL COMMENT 'Номер модуля',
  `module_model_id` int(11) DEFAULT NULL COMMENT 'Номер модели модуля',
  `template` text NOT NULL COMMENT 'Шаблон вывода'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица меню' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `t_kg_menu`
--

INSERT INTO `t_kg_menu` (`id`, `parent_id`, `name`, `alt_name`, `url`, `seo_title`, `seo_keywords`, `seo_description`, `group_id`, `position`, `level`, `module_id`, `module_model_id`, `template`) VALUES
(1, NULL, 'О премии', 'o_premii', 'about-award', 'О премии', 'О премии', 'О премии', 1, 1, 0, 3, 3, ''),
(2, NULL, 'Номинация', 'nominacija', 'nomination', 'Номинация', 'Номинация', 'Номинация', 1, 2, 0, 3, 6, ''),
(3, NULL, 'Жюри конкурса', 'zhjuri_konkursa', 'jury', 'Жюри конкурса', 'Жюри конкурса', 'Жюри конкурса', 1, 3, 0, 4, 4, ''),
(4, NULL, 'Новости', 'novosti', 'news', 'Новости', 'Новости', 'Новости', 1, 4, 0, 1, NULL, ''),
(5, NULL, 'Партнеры', 'partnery', 'partners', 'Партнеры', 'Партнеры', 'Партнеры', 1, 5, 0, 5, 5, ''),
(6, NULL, 'Регистрация', 'registracija', 'registration', 'Регистрация', 'Регистрация', 'Регистрация', 1, 6, 0, 3, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_menu_group`
--

CREATE TABLE `t_kg_menu_group` (
`id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `position` int(11) NOT NULL,
  `section_position` int(11) NOT NULL,
  `active_status` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица - группа меню' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `t_kg_menu_group`
--

INSERT INTO `t_kg_menu_group` (`id`, `name`, `position`, `section_position`, `active_status`) VALUES
(1, 'Главное меню', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_migration`
--

CREATE TABLE `t_kg_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_kg_migration`
--

INSERT INTO `t_kg_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1399934613),
('m130524_201442_init', 1399934617),
('m140512_224206_m130524_201442_init', 1399934617);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_module`
--

CREATE TABLE `t_kg_module` (
`id` int(11) NOT NULL,
  `name` varchar(512) DEFAULT NULL COMMENT 'Название',
  `module_name` varchar(512) DEFAULT NULL COMMENT 'Модуль',
  `object_name` varchar(512) DEFAULT NULL COMMENT 'Модель',
  `subentries` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Вложенные записи'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Модули' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `t_kg_module`
--

INSERT INTO `t_kg_module` (`id`, `name`, `module_name`, `object_name`, `subentries`) VALUES
(-1, 'Главная страница', NULL, NULL, 0),
(1, 'Cообщение (Новости, Статьи и т.д.)', 'post', 'Post', 0),
(3, 'Страницы', 'pages', 'common\\modules\\pages\\models\\Pages', 1),
(4, 'Жюри', 'jury', 'common\\modules\\jury\\models\\Jury', 0),
(5, 'Партнеры', 'partners', 'common\\modules\\partners\\models\\Partners', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_nomination`
--

CREATE TABLE `t_kg_nomination` (
`id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL COMMENT 'Категория',
  `name` varchar(512) NOT NULL COMMENT 'Название',
  `link` varchar(512) NOT NULL COMMENT 'Ссылка',
  `position` int(11) NOT NULL DEFAULT '99' COMMENT 'Позиция отображения',
  `intro` text NOT NULL COMMENT 'Краткое описание',
  `text` mediumtext NOT NULL COMMENT 'Описание номинации',
  `seo_title` varchar(512) NOT NULL COMMENT 'SEO Заголовок',
  `seo_keywords` varchar(512) NOT NULL COMMENT 'SEO Ключевые слова',
  `seo_description` text NOT NULL COMMENT 'SEO Описание'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Список наминаций' AUTO_INCREMENT=14 ;

--
-- Dumping data for table `t_kg_nomination`
--

INSERT INTO `t_kg_nomination` (`id`, `category_id`, `name`, `link`, `position`, `intro`, `text`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(1, 1, 'Мультибрендовые и мультикатегорийные компании', '', 1, '<p>Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России.Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России.Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России.Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России.</p>', '', 'Мультибрендовые и мультикатегорийные компании', 'Мультибрендовые и мультикатегорийные компании', 'Мультибрендовые и мультикатегорийные компании'),
(2, 1, 'Продукты питания', '', 2, '<p>Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России.</p>', '', 'Продукты питания', 'Продукты питания', 'Продукты питания'),
(3, 1, 'Отдых, развлечения, туризм, культура, спорт, образование', '', 3, '<p>Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России.</p>', '', 'Отдых, развлечения, туризм, культура, спорт, образование', 'Отдых, развлечения, туризм, культура, спорт, образование', 'Отдых, развлечения, туризм, культура, спорт, образование'),
(4, 1, 'Услуги для бизнеса', '', 4, '', '', 'Услуги для бизнеса', 'Услуги для бизнеса', 'Услуги для бизнеса'),
(5, 1, 'Товары для дома', '', 5, '', '', 'Товары для дома', 'Товары для дома', 'Товары для дома'),
(6, 1, 'Банки, страховые компании, строительство телекоммуникаций, интернет и другие услуги связи', '', 6, '', '', 'Банки, страховые компании, строительство телекоммуникаций, интернет и другие услуги связи', 'Банки, страховые компании, строительство телекоммуникаций, интернет и другие услуги связи', 'Банки, страховые компании, строительство телекоммуникаций, интернет и другие услуги связи'),
(7, 1, 'Одежда, обувь, украшения', '', 7, '', '', 'Одежда, обувь, украшения', 'Одежда, обувь, украшения', 'Одежда, обувь, украшения'),
(8, 1, 'Здоровье, средства по уходу, товары гигиены', '', 8, '', '', 'Здоровье, средства по уходу, товары гигиены', 'Здоровье, средства по уходу, товары гигиены', 'Здоровье, средства по уходу, товары гигиены'),
(9, 2, 'Успешный реaрендинг', '', 1, '<p>С 2001 года российская национальная награда в области построения брэндов Брэнд года входит в международную систему Effie — самую авторитетную мировую награду в сфере эффективных маркетинговых коммуникаций проводимую в 33-х странах мира.<span></span></p>', '<p><span></span></p>', 'Успешный реaрендинг', 'Успешный реaрендинг', 'Успешный реaрендинг'),
(10, 2, 'Бренд без бюджета', '', 2, '<p>Effie является единственной национальной наградой способной оценить успехи компании во взаимодействи с целевой аудиторией, а также достигнутые результаты. Помимо национальных программ Effie существует конкурс Effie Европа и глобальный Effie.<span></span></p>', '<p><span></span></p>', 'Бренд без бюджета', 'Бренд без бюджета', 'Бренд без бюджета'),
(11, 2, 'Корпоративный бренд', '', 3, '<p>Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России.<span></span></p>', '', 'Корпоративный бренд', 'Корпоративный бренд', 'Корпоративный бренд'),
(12, 2, 'Инновационный бренд', '', 4, '<p>Награда Effie вручается за главное достижение в сфере рекламы и маркетинговых коммуникации — эффективность. Организованный в 1968 году Нью-Йоркской Маркетинговой Ассоциацией, конкурс Effie является единственной национальной наградой способной оценить успехи компании.<span></span></p>', '', 'Инновационный бренд', 'Инновационный бренд', 'Инновационный бренд'),
(13, 2, 'Социальный бренд / СКО', '', 5, '<p>Стремясь отразить развитие маркетинговых компаний, Effie объявляет о начале первого этапа многолетнего плана развития Конкурса. Начиная с этой осени, заявки на участие в конкурсе включают четыре новые категории, которые признают многие из оригинальных креативных идей и коммуникационных подходов, способствующих эффективности кампании.<span></span></p>', '', 'Социальный бренд / СКО', 'Социальный бренд / СКО', 'Социальный бренд / СКО');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_nomination_category`
--

CREATE TABLE `t_kg_nomination_category` (
`id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL COMMENT 'Название'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Категория номинации' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t_kg_nomination_category`
--

INSERT INTO `t_kg_nomination_category` (`id`, `name`) VALUES
(1, 'Основной конкурс'),
(2, 'Специальные номинации');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_pages`
--

CREATE TABLE `t_kg_pages` (
`id` int(11) NOT NULL,
  `title` varchar(512) NOT NULL,
  `file_name` varchar(512) NOT NULL COMMENT 'Название файла',
  `text` longtext NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Статические страницы' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `t_kg_pages`
--

INSERT INTO `t_kg_pages` (`id`, `title`, `file_name`, `text`) VALUES
(3, 'О премии', 'o_premii', '<?php\r\n/**\r\n * Created by PhpStorm.\r\n * User: developer\r\n * Date: 25.06.14\r\n * Time: 15:27\r\n */\r\n\r\n\r\n$this->title = $menu->seo_title." - ".Yii::$app->params[''seo_title''];\r\nYii::$app->view->registerMetaTag([''name'' => ''keywords'', ''content'' => $menu->seo_keywords]);\r\nYii::$app->view->registerMetaTag([''name'' => ''description'', ''content'' => $menu->seo_description]);\r\n?>\r\n<div style="position: relative;">\r\n<?php\r\n//\\common\\widget\\html\\ActiveEdit::begin([''model'' => $model, ''url'' => "/pages/simple-edit/edit"]);\r\n?>\r\n<section class="about">\r\n<div class="container container-text">\r\n	<?=\\common\\modules\\contentBlock\\widget\\ContentBlockWidget::widget([\r\n        ''id'' => 12\r\n    ]);?>\r\n</div>\r\n</section><section class="levels">\r\n<div class="container">\r\n	<div class="row">\r\n		<strong class="title">Этапы 2014</strong>\r\n		<div class="row levels-list-block">\r\n			<?php\r\n            $stages = \\common\\modules\\stage\\models\\Stage::find()->all();\r\n            foreach ($stages as $index => $stage) {\r\n                ?>\r\n                <div class="col-xs-6 col-sm-4 col-md-3 level <?=$stage->past_stage == 1  ? "disabled" : ""?> <?=$stage->current_stage == 1  ? "current" : ""?>">\r\n                    <h2><?=$stage->number?></h2>\r\n                    <span class="date <?=(count($stages) == ($index + 1) ? "t-g" : "")?>"><?=$stage->date?></span>\r\n                    <a class="title"><?=$stage->title?></a>\r\n                    <span class="stat"><?=($stage->past_stage == 1 ? "Этап завершен" : $stage->note)?></span>\r\n                </div>\r\n            <?php\r\n            }\r\n            ?>\r\n		</div>\r\n	</div>\r\n</div>\r\n</section><section class="about">\r\n<div class="container container-text">\r\n	<?=\\common\\modules\\contentBlock\\widget\\ContentBlockWidget::widget([\r\n        ''id'' => 8\r\n    ]);?>\r\n</div>\r\n</section>\r\n<?php\r\n//\\common\\widget\\html\\ActiveEdit::end();\r\n?>\r\n</div>\r\n'),
(4, 'Жюри конкурса', 'zhjuri_konkursa', '<section class="jury">\r\n<div class="container">\r\n	<div class="member">\r\n		<div class="row">\r\n			<div class="col-md-3">\r\n				<img src="/uploads/1/jury1.jpg">\r\n			</div>\r\n			<div class="col-md-9">\r\n				<h2>Дерк Сауэр</h2>\r\n				<p>\r\n					Дерк Сауэр является одним из самых влиятельных медиаменеджеров России. Родился 31\r\nоктября 1952 года в Амстердаме (Нидерланды). В 1970-8 0-х гг. работал корреспондентом\r\nтелекомпаний «Belfast» и «Title Filme». Состоял в должности редактора журналов «Twingtig»\r\nи «Neiuwe Revu» (Нидерланды). В 1990 году г-н Сауэр основал первый в СССР глянцевый\r\nжурнал «Moscow Magazine» и газету «Moscow Guardian». В 1992 году основал издательский дом\r\n«Independent Media», который возглавлял в качестве генерального директора до 2008 года.\r\nВ начале 2005 года продал долю в «Independent Media» финскому медиахолдингу\r\n«SanomaWSOY».\r\n				</p>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class="member">\r\n		<div class="row">\r\n			<div class="col-md-3">\r\n				<img src="/uploads/1/jury2.jpg">\r\n			</div>\r\n			<div class="col-md-9">\r\n				<h2>Роман Баданин</h2>\r\n				<p>\r\n					Руководит сайтом Rbc.Ru с января 2014 года. В течение 10 лет был заместителем главного\r\nредактора сайта Gazeta. Ru. С декабря 2011 года Роман Баданин стал шеф-редактором сайта\r\nForbes.Ru. С осени 2013 года – исполнительным директором службы интернет-проектов\r\nагентства «Интерфакс».\r\n				</p>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class="member">\r\n		<div class="row">\r\n			<div class="col-md-3">\r\n				<img src="/uploads/1/jury3.jpg">\r\n			</div>\r\n			<div class="col-md-9">\r\n				<h2>Ольга подойницына</h2>\r\n				<p>\r\n					Ольга отвечает за разработку и реализацию коммуникационной и маркетинговой стратегии,\r\nа также за управление коммуникационной платформой ВТБ Капитал, инвестиционного бизнеса\r\nГруппы ВТБ, включая Европу, Азию, Ближний Восток и США. С момента основания ВТБ Капитал\r\nв 2008 году, Ольга сыграла ключевую роль в становлении компании от нового игрока рынка\r\nк признанному бренду в международном инвестиционно-банковском сообществе.  Ольга\r\nудостоена профессиональных наград. В 2014 году она стала первым российским\r\nпредставителем, получившим премию SABRE Award за выдающиеся профессиональные\r\nдостижения. SABRE Award — крупнейшая международная премия в сфере связей\r\nс общественностью. Также в 2014 году команда Департамента корпоративных отношений\r\nи маркетинга ВТБ Капитал была названа «Лучшей командой на рынке финансовых\r\nкоммуникаций» и получила премию Corporate &amp; Financial Award, одну из наиболее\r\nавторитетных наград в сфере коммуникаций.\r\n				</p>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n</section>'),
(5, 'Партнеры', 'partnery', '<?php\r\n/**\r\n * Created by PhpStorm.\r\n * User: developer\r\n * Date: 25.06.14\r\n * Time: 15:27\r\n */\r\n\r\n\r\n$this->title = $menu->seo_title." - ".Yii::$app->params[''seo_title''];\r\nYii::$app->view->registerMetaTag([''name'' => ''keywords'', ''content'' => $menu->seo_keywords]);\r\nYii::$app->view->registerMetaTag([''name'' => ''description'', ''content'' => $menu->seo_description]);\r\n?>\r\n\r\n<?=\\common\\modules\\contentBlock\\widget\\ContentBlockWidget::widget([\r\n    ''id'' => 9\r\n]);?>\r\n\r\n'),
(6, 'Номинации', 'nomination', '<?php\r\n/**\r\n * Created by PhpStorm.\r\n * User: developer\r\n * Date: 25.06.14\r\n * Time: 15:27\r\n */\r\n\r\nuse common\\helpers\\CString;\r\n\r\n$this->title = $menu->seo_title." - ".Yii::$app->params[''seo_title''];\r\nYii::$app->view->registerMetaTag([''name'' => ''keywords'', ''content'' => $menu->seo_keywords]);\r\nYii::$app->view->registerMetaTag([''name'' => ''description'', ''content'' => $menu->seo_description]);\r\n?>\r\n<div class="container">\r\n<?=\\common\\modules\\contentBlock\\widget\\ContentBlockWidget::widget([\r\n    ''id'' => 10\r\n]);?>\r\n</div>\r\n<section class="nominees" style="margin: 90px 0px 0px;">\r\n<div class="container" >\r\n	<h2>Основной конкурс</h2>\r\n	<div class="row">\r\n	    <?php\r\n	    $nomination_category_1  = \\common\\modules\\nomination\\models\\Nomination::find()->where([''category_id'' => 1])->all();\r\n	    \r\n	    foreach($nomination_category_1 as $item) {\r\n	        ?>\r\n	        <div class="col-md-3 item">\r\n    			\r\n    			<h3><?=$item->name?></h3>\r\n    			<?=CString::typography($item->intro)?>\r\n    		</div>\r\n	        <?php\r\n	    }\r\n	    ?>\r\n	</div>\r\n</div>\r\n</section>\r\n<section class="nominees">\r\n<div class="container">\r\n	<h2>Специальные номинации</h2>\r\n	<?php\r\n	    $nomination_category_1  = \\common\\modules\\nomination\\models\\Nomination::find()->where([''category_id'' => 2])->all();\r\n	    \r\n	    foreach($nomination_category_1 as $item) {\r\n	        ?>\r\n	        <div class="col-md-12 item">\r\n        		<h3><?=$item->name?></h3>\r\n        		<?=CString::typography($item->intro)?>\r\n        	</div>\r\n	        <?php\r\n	    }\r\n	    ?>\r\n</div>\r\n</section>');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_participant_of_competition`
--

CREATE TABLE `t_kg_participant_of_competition` (
`id` int(11) NOT NULL,
  `brand_name` varchar(512) NOT NULL COMMENT 'Название брэнда',
  `nomination_id` int(11) NOT NULL COMMENT 'Номинация',
  `seminar` int(11) NOT NULL COMMENT 'Участвовать в семинаре',
  `first_name` varchar(512) NOT NULL COMMENT 'Имя',
  `last_name` varchar(512) NOT NULL COMMENT 'Фамилия',
  `job` varchar(512) NOT NULL COMMENT 'Должность',
  `phone` varchar(512) NOT NULL COMMENT 'Номер телефона',
  `email` varchar(512) NOT NULL COMMENT 'Электронная почта',
  `reg_date` int(11) NOT NULL COMMENT 'Дата регистрации',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Статус заявки'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица участников конкурса' AUTO_INCREMENT=65 ;

--
-- Dumping data for table `t_kg_participant_of_competition`
--

INSERT INTO `t_kg_participant_of_competition` (`id`, `brand_name`, `nomination_id`, `seminar`, `first_name`, `last_name`, `job`, `phone`, `email`, `reg_date`, `status`) VALUES
(30, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1422971826, 0),
(31, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r2@kagama.ru', 1422972030, 0),
(32, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r2@kagama.ru', 1422972030, 0),
(33, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r2@kagama.ru', 1422972030, 0),
(34, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r2@kagama.ru', 1422972151, 0),
(35, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1422972537, 0),
(36, 'Кагама', 3, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1422973822, 0),
(37, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1422973852, 0),
(38, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1422974010, 0),
(39, 'Кагама', 5, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1422974186, 0),
(40, 'Кагама', 5, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1422974567, 0),
(41, 'Кагама', 7, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423046118, 0),
(42, 'Кагама', 8, 1, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423227404, 0),
(43, 'Тест', 3, 0, 'Тест', 'Тест', 'Тест', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423251974, 1),
(44, 'Кагама', 3, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423252956, 0),
(45, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423252989, 0),
(46, 'Кагама', 13, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253039, 0),
(47, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253102, 0),
(48, 'Кагама', 6, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253223, 0),
(49, 'Кагама', 2, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253317, 0),
(50, 'Кагама', 1, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253438, 0),
(51, 'Кагама', 6, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253605, 0),
(52, 'Кагама', 3, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253649, 0),
(53, 'Кагама', 2, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253780, 0),
(54, 'Кагама', 9, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253833, 0),
(55, 'Кагама', 10, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253882, 0),
(56, 'Кагама', 11, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423253947, 0),
(57, 'Кагама', 4, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423254016, 0),
(58, 'Кагама', 3, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423254269, 0),
(59, 'Кагама', 11, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423254316, 0),
(60, 'Кагама', 8, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423254397, 0),
(61, 'Кагама', 10, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423254510, 0),
(62, 'Кагама', 11, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423254568, 0),
(63, 'Кагама', 8, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423484975, 0),
(64, 'Кагама', 8, 0, 'Рашид', 'Абдулаев', 'CEO', '+7 (964) 019 5020', 'abdulaev.r@kagama.ru', 1423485085, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_partners`
--

CREATE TABLE `t_kg_partners` (
`id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL COMMENT 'Название',
  `logo` varchar(512) NOT NULL COMMENT 'Логотип',
  `logo_src` varchar(512) NOT NULL COMMENT 'Путь к лого',
  `description` text NOT NULL COMMENT 'Описание',
  `position` int(11) NOT NULL DEFAULT '99' COMMENT 'Позиция отображения',
  `url` varchar(512) DEFAULT NULL COMMENT 'Ссылка на сайт компании'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица партнеры' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `t_kg_partners`
--

INSERT INTO `t_kg_partners` (`id`, `name`, `logo`, `logo_src`, `description`, `position`, `url`) VALUES
(1, 'Mersedez-benz', 'af92133fafc71ba57418300af8438106.jpg', 'images/modules/partners/Mersedez_benz', '<p>\r\nМБ-Беляево, официальный дилер «Мерседес - Бенц» в Москве, успешно работает на российском рынке с 1992 года. Основополагающими принципами, которые обеспечили дилерскому центру репутаци солидного партнера и доверие клиентов, являются высокий профессионализм сотрудников компании и высокое качество предлагаемых услуг.\r\n</p>', 1, 'http://www.mercedes-benz.ru'),
(2, 'ОАО Газпром', 'b92dee081f9d557f69053072cda2f3af.jpg', 'images/modules/partners/oao_gazprom', '<p>ОАО «Газпром» — глобальная энергетическая компания. Основные направления деятельности — геологоразведка, добыча, транспортировка, хранение, переработка и реализация газа, газового конденсата и нефти, реализация газа в качестве моторного топлива, а также производство и сбыт тепло- и электроэнергии. </p>', 0, ''),
(3, 'Телеканал "Дождь"', '19b7cf83f3ba48530f14f8deb9412d3b.jpg', 'images/modules/partners/telekanal_dozhd', '<p>\r\nМБ-Беляево, официальный дилер «Мерседес - Бенц» в Москве, успешно работает на российском рынке с 1992 года. Основополагающими принципами, которые обеспечили дилерскому центру репутаци солидного партнера и доверие клиентов, являются высокий профессионализм сотрудников компании и высокое качество предлагаемых услуг. \r\n</p>', 3, ''),
(4, 'ОАО "Сбербанк"', 'ef9c6f888036857c33e265ff912733b0.jpg', 'images/modules/partners/oao_sberbank', '<p>ОАО «Газпром» — глобальная энергетическая компания. Основные направления деятельности — геологоразведка, добыча, транспортировка, хранение, переработка и реализация газа, газового конденсата и нефти, реализация газа в качестве моторного топлива, а также производство и сбыт тепло- и электроэнергии. </p>', 4, ''),
(5, 'Банк ВТБ 24', 'c26255ee03e3c11f36029c68b34e6bd3.jpg', 'images/modules/partners/bank_vtb_24', '<p>МБ-Беляево, официальный дилер «Мерседес - Бенц» в Москве, успешно работает на российском рынке с 1992 года. Основополагающими принципами, которые обеспечили дилерскому центру репутаци солидного партнера и доверие клиентов, являются высокий профессионализм сотрудников компании и высокое качество предлагаемых услуг. </p>', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_partners_category`
--

CREATE TABLE `t_kg_partners_category` (
`id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL COMMENT 'Название',
  `position` int(11) NOT NULL DEFAULT '99' COMMENT 'Позиция отображения'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Категории партнеров' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t_kg_partners_category`
--

INSERT INTO `t_kg_partners_category` (`id`, `name`, `position`) VALUES
(1, 'Генеральные партнеры премии', 1),
(2, 'Информационные партнеры премии', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_partners_category_relation`
--

CREATE TABLE `t_kg_partners_category_relation` (
  `partner_id` int(11) NOT NULL COMMENT 'Партнер',
  `category_id` int(11) NOT NULL COMMENT 'Категория'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Связующая таблица партнеров и категорий';

--
-- Dumping data for table `t_kg_partners_category_relation`
--

INSERT INTO `t_kg_partners_category_relation` (`partner_id`, `category_id`) VALUES
(3, 1),
(4, 1),
(6, 2),
(1, 1),
(5, 2),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_photo`
--

CREATE TABLE `t_kg_photo` (
`id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL COMMENT 'Название фото',
  `src` varchar(512) NOT NULL COMMENT 'Путь к фото',
  `gallery_id` int(11) NOT NULL COMMENT 'Галерея',
  `is_main` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Фотографии' AUTO_INCREMENT=23 ;

--
-- Dumping data for table `t_kg_photo`
--

INSERT INTO `t_kg_photo` (`id`, `name`, `src`, `gallery_id`, `is_main`) VALUES
(16, 'adfa53351a1cba2eaa0f84ab3bdd09fe.png', 'images/modules/gallery/20', 20, 1),
(17, '5725d7149eee3be0a732c0443f599589.png', 'images/modules/gallery/20', 20, 1),
(18, 'c8eb1b85b4d5706f60d65277b67dcc00.png', 'images/modules/gallery/20', 20, 1),
(19, '7f8759104217264b0c0e53575a28420b.png', 'images/modules/gallery/20', 20, 1),
(20, 'e967d6d227909762bdc589e4f89c05f3.png', 'images/modules/gallery/20', 20, 1),
(21, '59701e26e7eb7264415a9936fd4d0a53.png', 'images/modules/gallery/20', 20, 0),
(22, '7cc35ff2d45d1f400488249fdd8572b7.jpg', 'images/modules/gallery/20', 20, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_photogallery`
--

CREATE TABLE `t_kg_photogallery` (
`id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL COMMENT 'Название',
  `description` text NOT NULL COMMENT 'Описание',
  `seo_title` varchar(512) NOT NULL COMMENT 'SEO Заголовок',
  `seo_keywords` varchar(512) NOT NULL COMMENT 'SEO Ключевые слова',
  `seo_description` text NOT NULL COMMENT 'SEO Описание'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Фото галерея ' AUTO_INCREMENT=21 ;

--
-- Dumping data for table `t_kg_photogallery`
--

INSERT INTO `t_kg_photogallery` (`id`, `name`, `description`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(20, 'Мероприятие №2', 'Мероприятие №1Мероприятие №1<span></span>', 'Мероприятие №2', 'Мероприятие №2', 'Мероприятие №4');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_post`
--

CREATE TABLE `t_kg_post` (
`id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL COMMENT 'Категория',
  `title` varchar(512) NOT NULL COMMENT 'Заголовок',
  `alt_title` varchar(512) NOT NULL COMMENT 'Альт заголовок',
  `small_text` text NOT NULL COMMENT 'краткое описание',
  `text` longtext NOT NULL COMMENT 'плоный текст',
  `date` int(11) NOT NULL COMMENT 'дата',
  `seo_title` varchar(512) NOT NULL,
  `seo_keywords` varchar(512) NOT NULL,
  `seo_description` text NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Пользователь',
  `publish` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Публиковать',
  `views_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Количество просмотров',
  `author` varchar(512) NOT NULL COMMENT 'Автор',
  `source` varchar(512) NOT NULL COMMENT 'Источник'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица новостей' AUTO_INCREMENT=12 ;

--
-- Dumping data for table `t_kg_post`
--

INSERT INTO `t_kg_post` (`id`, `menu_id`, `title`, `alt_title`, `small_text`, `text`, `date`, `seo_title`, `seo_keywords`, `seo_description`, `user_id`, `publish`, `views_count`, `author`, `source`) VALUES
(4, 4, 'В Москве состоялась встреча вице-премьера Абусупьяна Хархарова с руководством компании Microsoft.', 'v_moskve_sostojalas_vstrecha_vice_premera_abusupjana_kharkharova_s_rukovodstvom_kompanii_Microsoft_', '<p>\r\n	<img style="height: 174.087px; width: 278px; float: left; margin: 0px 10px 10px 0px;" rel="height: 174.087px; width: 278px; float: left; margin: 0px 10px 10px 0px;" alt="" src="/uploads/1/waterreflections1440x90013033.jpg">\r\n</p><p style="text-align: center;">\r\n	Технологическом Центре компании прошла встреча заместителя председателя Правительства РД Абусупьян Хархарова с представит\r\n</p><p style="text-align: center;">\r\n	елями\r\n</p><p>\r\n	<br>\r\n</p><p>\r\n	<br>\r\n</p><p>\r\n	<br>\r\n</p><p>\r\n	<br>\r\n</p><p>\r\n</p><p>\r\n	Microsoft. Состоялась она в столице России. Встреча получилась продуктивной – удалось договориться о внедрении современных информационных технологий и сервисов в рамках реализации проекта под названием.\r\n</p><p>\r\n	<img style="height: 143px; width: 143px; float: right; margin: 0px 0px 10px 10px;" rel="height: 143px; width: 143px; float: right; margin: 0px 0px 10px 10px;" alt="" src="/uploads/1/t1.jpg">\r\n</p><p>\r\n	Они будут направлены на повышение производительности труда сотрудников Администрации Главы и Правительства РД.\r\n</p>', '<p>\r\n	<img style="height: 565px; width: 904px; display: block; margin: auto;" rel="height: 565px; width: 904px; display: block; margin: auto;" alt="" src="/uploads/1/46422308.jpg">\r\n</p><p>\r\n	Технологическом Центре компании прошла встреча заместителя председателя Правительства РД Абусупьян Хархарова с представителями Microsoft. Состоялась она в столице России. Встреча получилась продуктивной – удалось договориться о внедрении современных информационных технологий и сервисов в рамках реализации проекта под названием. Они будут направлены на повышение производительности труда сотрудников Администрации Главы и Правительства РД.\r\n</p><p><br>\r\n	</p><p>Для членов Правительства РД будет организован семинар, где их будут информировать об основах работы с инновационными продуктами и технологиями Microsoft. Планируется также организовать сотрудничество в программе Microsoft IT Academy на базе Юридической Академии. На этой же встрече Хархарову были продемонстрированы технологические возможности компании Microsoft Lynс  для организации видеоконференц-связи для органов государственной власти.\r\n</p><p>\r\n	<img style="display: block; margin: auto;" rel="display: block; margin: auto;" alt="" src="/uploads/1/t2.jpg">\r\n</p><p>\r\n	Была отмечена важность разработки единой концепции построения ИТ-инфраструктуры «Электронного Правительства», помимо этого обсуждался такой вариант, как обучение технологиям Microsoft сотрудников Администрации Главы и Правительства Республики Дагестан. Мало того, шанс получить такое обучение, есть и у школьников и студентов дагестанских вузов. Условия единого лицензионного соглашения в интересах органов власти республики, были так же обговорены на встрече. Представители Microsoft взяли на себя выявление потребностей в информационно-коммуникационных технологиях органов исполнительной власти Республики Дагестан.\r\n</p><p>\r\n	Обсудили также возможность организации ситуационного центра Главы республики, заключили в Управлении пресс-службы и информации Администрации Главы и Правительства РД.\r\n</p><p>\r\n	Церемония подписания документа запланировано провести на XII Сочинском инвестиционном форуме, который состоится в сентябре 2014 года.\r\n</p>', 1415131200, 'В Москве состоялась встреча вице-премьера Абусупьяна Хархарова с руководством компании Microsoft.', 'В Москве состоялась встреча вице-премьера Абусупьяна Хархарова с руководством компании Microsoft.', 'В Москве состоялась встреча вице-премьера Абусупьяна Хархарова с руководством компании Microsoft.', 6, 1, 8, 'Абдулаев Рашид Магомедович', 'riadagestan.ru'),
(5, 4, 'На дагестанском полигоне «Аданак» начались учения морпехов', 'na_dagestanskom_poligone_adanak_nachalis_uchenija_morpekhov', '<p>В Дагестане на полигоне «Аданак» морские пехотинцы Каспийской флотилии ВМФ проводят батальонные тактические учения с высадкой морского десанта, сообщили РИА «Дагестан» в пресс-службе Южного военного округа. wsw sf sdfsd fsdf </p>', '<p><img alt="" style="height: 284px; width: 454px; float: left; margin: 0px 10px 10px 0px;" src="/uploads/1/waterreflections1440x90013033.jpg"></p><p>В Дагестане на полигоне «Аданак» морские пехотинцы Каспийской флотилии ВМФ проводят батальонные тактические учения с высадкой морского десанта, сообщили РИА «Дагестан» в пресс-службе Южного военного округа.<br> <br> К учениям привлечено более 500 военнослужащих, задействованы малые артиллерийские корабли «Волгодонск» и «Махачкала», десантный катер «Атаман Платов», десантные катера типа «Серна» и около 50 единиц боевой и специальной техники.<br> <br> «По замыслу учения, батальону была поставлена задача захватить пункт высадки морского десанта, перейти к обороне и удерживать стратегически важный плацдарм, не дав условному противнику осуществить высадку на побережье.<br> <br> Морские пехотинцы были подняты по тревоге, совершили марш в район формирования колонн и погрузились на десантные катера», – говорится в тексте сообщения.<br> <br> Необходимо отметить, что батальонное тактическое учение проводится в соответствии с утвержденным планом деятельности Каспийской флотилии на 2014 учебный год и завершает этап подготовки береговых войск флотилии в летнем периоде обучения.<br> <br> По данным ведомства, учения продлятся 5 суток.</p>', 1414440000, 'На дагестанском полигоне «Аданак» начались учения морпехов', 'На дагестанском полигоне «Аданак» начались учения морпехов', 'На дагестанском полигоне «Аданак» начались учения морпехов', 0, 1, 2, '', ''),
(7, 4, 'Глава Минстроя РД принял участие в ХIII Общероссийском форуме по стратегическому планированию в регионах и городах России', 'glava_minstroja_rd_prinjal_uchastie_v_khIII_obshherossijjskom_forume_po_strategicheskomu_planirovaniju_v_regionakh_i_gorodakh_rossii', '<p>\r\n	Министр строительства, ар<a href="http://www.kagama.ru">хитектуры и ЖКХ РД Муса Мусаев принял участие в ХIII Общероссийском форуме по стратегическому планированию в регионах и городах России, сообщили РИА «Дагестан» в пресс-службе Минстроя РД</a>\r\n</p>', '<p><br></p><iframe style="width: 500px; height: 281px;" src="//www.youtube.com/embed/S_xH7noaqTA" allowfullscreen="" frameborder="0"></iframe><p><br></p><p><br></p><p></p><p>По словам руководителя ведомства, форум был организован Госдумой совместно с министерством финансов РФ и Минэкономразвития России. Проходило мероприятие в Санкт-Петербурге.<br> <br> «<a href="http://www.kagama.ru">Главной задачей его, по словам самих организаторов, является содействие гармоничному, сбалансированному развитию регионов и городов России путем формирования системы территориального и с</a>тратегического планирования, улучшения координации, поддержания многостороннего диалога по поводу долгосрочных приоритетов развития, создания и продвижения передовых стандартов планирования», – сказал Мусаев.<br> </p><p><br></p><iframe style="width: 500px; height: 281px;" src="//www.youtube.com/embed/S_xH7noaqTA" allowfullscreen="" frameborder="0"></iframe><p><br> Руководитель Минстроя республики также принял участие в "круглом столе" «Открытое заседание Межведомственной рабочей группы по социально-экономическому развитию городских агломераций в Российской Федерации», который проводился в рамках форума.<br> <br> Напомним, Общероссийский форум «Стратегическое планирование в регионах и городах России» проводится ежегодно с 2002 года. Он твердо закрепил за собой право считаться основной площадкой для обсуждения методов и механизмов стратегического планирования, для конструктивной дискуссии по самым важным и острым вопросам реализации стратегических планов и комплексных проектов развития городов и регионов Российской Федерации.</p>', 1415217600, 'Глава Минстроя РД принял участие в ХIII Общероссийском форуме по стратегическому планированию в регионах и городах России', 'Глава Минстроя РД принял участие в ХIII Общероссийском форуме по стратегическому планированию в регионах и городах России', 'Глава Минстроя РД принял участие в ХIII Общероссийском форуме по стратегическому планированию в регионах и городах России', 0, 1, 3, '', ''),
(8, 4, 'Арбитражный суд Республики Дагестан признал банкротом махачкалинский банк «Эсид»', 'arbitrazhnyjj_sud_respubliki_dagestan_priznal_bankrotom_makhachkalinskijj_bank_ehsid', '<p>Арбитражный суд Республики Дагестан признал банкротом ООО коммерческий банк «Эсид», сообщили РИА «Дагестан» в пресс-службе банка.<br> <br> Источник в суде отметил, что в отношении банка введено наблюдение, конкурсным управляющим назначена Государственная корпорация «Агентство по страхованию вкладов».</p>', '<p>Арбитражный суд Республики Дагестан признал банкротом ООО коммерческий банк «Эсид», сообщили РИА «Дагестан» в пресс-службе банка.<br> <br> Источник в суде отметил, что в отношении банка введено наблюдение, конкурсным управляющим назначена Государственная корпорация «Агентство по страхованию вкладов».<br> <br> С таким требованием в начале октября в Арбитражный суд Республики Дагестан обратился Центральный Банк России в лице Отделения – Национального банка по Республике Дагестан Южного главного управления Центробанка РФ.<br> <br> Напомним, 30 сентября 2014 года Банк России отозвал у кредитной организации ООО «Эсидбанк» лицензию на осуществление банковских операций.<br> <br> В заявлении ЦБ РФ указал на неисполнение кредитной организацией ООО Коммерческий банк «Эсид» федеральных законов, регулирующих банковскую деятельность, а также нормативных актов Банка России, неоднократное нарушение ФЗ «О противодействии легализации (отмыванию) доходов, полученных преступным путем, и финансированию терроризма». <br> <br> Также Центробанк установил факты существенной недостоверности отчетных данных, достаточностью капитала ниже 2 %, снижение размера собственных средств (капитала) ниже минимального значения уставного капитала, а также неспособность удовлетворить требования кредиторов по денежным обязательствам.<br> <br> Также в заявлении указано на наличие задолженности по денежным обязательствам, не исполненным в срок, в сумме 1,76 миллиона рублей.</p>', 0, 'Арбитражный суд Республики Дагестан признал банкротом махачкалинский банк «Эсид»', 'Арбитражный суд Республики Дагестан признал банкротом махачкалинский банк «Эсид»', 'Арбитражный суд Республики Дагестан признал банкротом махачкалинский банк «Эсид»', 0, 1, 0, '', ''),
(9, 4, 'Вопросы защиты прав потребителей в сфере ЖКХ обсудили на заседании Консультативного совета в Махачкале ', 'voprosy_zashhity_prav_potrebitelejj_v_sfere_zhkkh_obsudili_na_zasedanii_konsultativnogo_soveta_v_makhachkale', '<p>Вопросы защиты прав потребителей в сфере жилищно-коммунального хозяйства обсудили на заседании Консультативного совета в Махачкале, сообщили РИА «Дагестан» в пресс-службе аппарата Уполномоченного по защите прав предпринимателей в РД.</p>', '<p>\r\n	Вопросы защиты прав потребителей в сфере жилищно-коммунального хозяйства обсудили на заседании Консультативного совета в Махачкале, сообщили РИА «Дагестан» в пресс-службе аппарата Уполномоченного по защите прав предпринимателей в РД.\r\n	<br>\r\n	<br>\r\n	 Мероприятие проходило под председательством руководителя Управления Роспотребнадзора по РД Э. Омариевой. От аппарата на встрече присутствовал консультант аппарата бизнес-омбудсмена республики Р. Кафланов.\r\n	<br>\r\n	<br>\r\n	 На заседании также присутствовали представители Управления Роспотребнадзора по РД, президент Ассоциации обществ защиты прав потребителей, председатель Региональной общественной организации «Комитет защиты прав потребителей» Х. Даниялов, заместитель главы администрации Махачкалы К. Изиев, руководитель Государственной жилищной инспекции А. Джабраилов, заместитель председателя Дагестанского регионального отделения общественной организации предпринимателей «Опора России» и другие.\r\n	<br>\r\n	<br>\r\n	 Касаясь темы деятельности жилищно-коммунальных хозяйств, представителем аппарата Уполномоченного по защите прав предпринимателей в РД был озвучен ряд проблем. «Часто поступают жалобы на необоснованное, по словам руководителей ТСЖ, наложение административных взысканий по итогам проверок санитарного состояния со стороны территориальных отделов Управления Роспотребнадзора по РД», – отметил Р. Кафланов.\r\n	<br>\r\n	<br>\r\n	 По \r\n	<u><a target="_blank" href="/uploads/11/positioneffierussia.docx">результатам</a></u> заседания министерствам, муниципальным учреждениям, а также общественным организациям были даны рекомендации для улучшения работы по защите прав потребителей при оказании жилищно-коммунальных услуг.\r\n</p>', 1415217600, 'Вопросы защиты прав потребителей в сфере ЖКХ обсудили на заседании Консультативного совета в Махачкале ', 'Вопросы защиты прав потребителей в сфере ЖКХ обсудили на заседании Консультативного совета в Махачкале ', 'Вопросы защиты прав потребителей в сфере ЖКХ обсудили на заседании Консультативного совета в Махачкале ', 0, 1, 3, '', ''),
(10, 4, 'Полная готовность к отопительному сезону в Дагестане ожидается 1 ноября', 'polnaja_gotovnost_k_otopitelnomu_sezonu_v_dagestane_ozhidaetsja_1_nojabrja', '<p>С 1 ноября ожидается полная готовность <a href="http://kagama.ru">к отопительному сезону в</a> Дагестане, заявил РИА «Дагестан» начальник отдела энергетики и возобновляемых источников энергии Агентства по энергетике РД Роберт Ильясов.<br> <br> По его словам, на данный момент все предприятия ЖКХ республики получили паспорта готовности к осенне-зимнему периоду 2014-2015 годов. «Общая готовность предприятий энергетики к ОЗП оценивается в 99,9%», – пояснил он.</p>', '<p>С 1 ноября ожидается полная готовность к отопительному сезону в Дагестане, заявил РИА «Дагестан» начальник отдела энергетики и возобновляемых источников энергии Агентства по энергетике РД Роберт Ильясов.<br> <br> По его словам, на данный момент все предприятия ЖКХ республики получили паспорта готовности к осенне-зимнему периоду 2014-2015 годов. «Общая готовность предприятий энергетики к ОЗП оценивается в 99,9%», – пояснил он.<br> <br> Стоит отметить, что по республике паспортизация объектов жилищно-коммунального сектора завершилась 15 октября. Еще в сентябре текущего года в дочернем обществе МРСК Северного Кавказа «Дагэнергосеть» и других подведомственных компаниях была сформирована комиссия по оценке готовности к работе в осенне-зимний период 2014-2015 годов. <br> <br> «Еще в начале октября, в городах и районах республики было подготовлено 4 млн 900 тысяч квадратных метров жилищного фонда, 428 котельных, 20 центральных тепловых пунктов, отремонтировано свыше 589 километров тепловых сетей, свыше 12 тыс. 867 километров водопроводных сетей, свыше 731 км канализационных сетей и 4 тыс. 400 км электрических сетей», – отметил начальник Госжилинспекции Дагестана Али Джабраилов.<br> <br> Руководитель Агентства по энергетике Дагестана Муртузали Гитинасулов подчеркнул, что одной из главных задач республики является обеспечение резервными источниками снабжения электроэнергией всех социально значимых объектов: школ, больниц, котельных и т.д.<br> <br> Глава ведомства констатировал, что при подготовке к ОЗП были произведены запасы в размере 159 тонн жидкого топлива (мазут) и 11 тыс. 377 тонн твердого топлива (уголь), сформирован резерв материально-технических ресурсов для ликвидации аварий и неисправностей на объектах ЖКХ на сумму около 30 млн 900 тыс. рублей. <br> <br> «Для механизированной уборки улично-дорожной сети от снега подготовлено 246 единиц спецтехники и необходимое количество антигололедных реагентов», – пояснил Гитинасулов.<br> <br> В то же время, по словам руководства энерго- и теплоснабжающих компаний республики, для комплектации аварийного запаса до 15 ноября будут получены материалы, и в дальнейшем совместно с УЖКХ столицы предполагается проводить выездные контрольные мероприятия на подстанциях.</p>', 1415217600, 'Полная готовность к отопительному сезону в Дагестане ожидается 1 ноября', 'Полная готовность к отопительному сезону в Дагестане ожидается 1 ноября', 'Полная готовность к отопительному сезону в Дагестане ожидается 1 ноября', 0, 1, 1, '', ''),
(11, 4, 'Госжилинспекция Дагестана разработала график дежурств в праздничные дни ', 'goszhilinspekcija_dagestana_razrabotala_grafik_dezhurstv_v_prazdnichnye_dni', '<p>Государственная жилищная инспекция РД разработала график дежурств в праздничные дни, сообщили РИА «Дагестан» в пресс-службе ГЖИ республики.</p>', '<p>Государственная жилищная инспекция РД разработала график дежурств в праздничные дни, сообщили РИА «Дагестан» в пресс-службе ГЖИ республики.<br> <br> «Дежурства будут организованы с 1 по 4 ноября в целях оперативного реагирования по вопросам предоставления жилищно-коммунальных услуг», – отметил источник.<br> <br> Главный государственный жилищный инспектор республики Али Джабраилов выразил пожелания всем жителям Дагестана провести День народного единства – единства всех российских народов в благоприятных и комфортных условиях. <br> <br> Во исполнение этого пожелания в праздничные дни граждане республики могут обратиться в Госжилинспекцию РД по предоставлению некачественных коммунальных услуг тепло-, водо-, электро-, газоснабжения, а также о недобросовестном отношении со стороны управляющих компаний и ресурсоснабжающих организаций по дежурному номеру 62-31-23.<br> <br> «Жители республики часто сталкиваются с проблемами предоставления коммунальных услуг, особенно это замечается в праздничные дни. Возникает необходимость оперативного реагирования на нужды людей, организацию дежурств в предстоящие праздничные и выходные дни», - комментирует руководитель Госжилинспекции РД.</p>', 1415217600, 'Госжилинспекция Дагестана разработала график дежурств в праздничные дни ', 'Госжилинспекция Дагестана разработала график дежурств в праздничные дни ', 'Госжилинспекция Дагестана разработала график дежурств в праздничные дни ', 0, 1, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_user`
--

CREATE TABLE `t_kg_user` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL COMMENT 'Пароль',
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `role_id` smallint(6) NOT NULL DEFAULT '1',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `approve_newsletter` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `t_kg_user`
--

INSERT INTO `t_kg_user` (`id`, `username`, `password`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role_id`, `status`, `created_at`, `updated_at`, `approve_newsletter`) VALUES
(1, 'rashid', '7d0ba610dea3dbcc848a97d8dfd767ae', '', '$2y$13$vdd8J1guykzyKtrcWY1l8O1MKHfbmPc4yfdDBXs.T76ZiK96BeHG2', NULL, '', 1, 1, 0, 0, 0),
(6, 'poramidol', 'poramidol', 'eN7iK5tfi_7of_qUSPPTP74I0hpb0lZo', '$2y$13$aXN3Yc3n/LD7JNMTvX/rQ.FVzFFKPhEFMTi8Dp.ceMfoMYpTfajcm', NULL, 'poramidol@mail.ru', 2, 1, 0, 1403372147, 0),
(7, 'dima', 'dima', '', '$2y$13$KlOfB50isT1rkqO/SIqnM.vek2qZXYY66R8XgZaxoEx..aUyBxewm', NULL, '', 1, 1, 0, 0, 0),
(11, 'dddd', '11ddbaf3386aea1f2974eee984542152', '', '$2y$13$Atue22as6Z2BZNzC6eAOnOmyw/U/T8MyE//0F9R47R7iLn3mloRlS', NULL, '', 1, 1, 0, 0, 0),
(12, 'cccc', '41fcba09f2bdcdf315ba4119dc7978dd', 'x]', '$2y$13$wkvTCyfAhY0VWewH/psS1e5SLTcFYaV3BK79.B/fTvea3bqrLdf7O', NULL, '', 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_user_role`
--

CREATE TABLE `t_kg_user_role` (
`id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL COMMENT 'Название роли'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Роль пользователя' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `t_kg_user_role`
--

INSERT INTO `t_kg_user_role` (`id`, `name`) VALUES
(1, 'Администратор'),
(2, 'Модератор'),
(3, 'Пользователь');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_winners`
--

CREATE TABLE `t_kg_winners` (
`id` int(11) NOT NULL,
  `place` varchar(32) NOT NULL COMMENT 'Место',
  `name` varchar(128) NOT NULL COMMENT 'Название',
  `description` text NOT NULL COMMENT 'Описание',
  `competition_id` int(11) NOT NULL COMMENT 'Конкурс'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица победители' AUTO_INCREMENT=19 ;

--
-- Dumping data for table `t_kg_winners`
--

INSERT INTO `t_kg_winners` (`id`, `place`, `name`, `description`, `competition_id`) VALUES
(1, 'gold', 'Икеа', 'Рекламная компания «Икея будит любовь»', 1),
(2, 'silver', 'Роллы Макдональд', 'Компания-заявитель: ООО «Огилви энд Мейзер» Владелец брэнда: ОАО СК «Альянс»', 1),
(3, 'bronze', 'Диана', 'Компания-заявитель: ЗАО «Инстинкт» Владелец брэнда: ООО «ИКЕА-ДОМ»', 1),
(4, 'gold', 'Smart News', 'Компания-заявитель: Московское Представительство Компании «Арима Холдинг Корп.» Владелец брэнда: ARIMA HOLDING CORPORATION', 2),
(5, 'silver', 'Cirque du Soleil', 'Компания-заявитель: ЗАО «Инстинкт» Владелец брэнда: ООО «ИКЕА-ДОМ»', 2),
(6, 'bronze', 'Hyundai EQUUS', 'Компания-заявитель: Московское Представительство Компании «Арима Холдинг Корп.» Владелец брэнда: ARIMA HOLDING CORPORATION', 2),
(7, 'gold', 'Мегафон', 'Компания-заявитель: ЗАО «Инстинкт» Владелец брэнда: ОАО «МегаФон»', 3),
(8, 'silver', 'Супер МТС', 'Компания-заявитель: ЗАО «Инстинкт» Владелец брэнда: ООО «ИКЕА-ДОМ»', 3),
(9, 'bronze', 'Allianz', 'Компания-заявитель: Московское Представительство Компании «Арима Холдинг Корп.» Владелец брэнда: ARIMA HOLDING CORPORATION', 3),
(13, 'gold', 'sd fasdf asdfasdf ads21212121', 'sd fasdf asdfasdf ads12121212', 4),
(14, 'silver', 'sd fasdf asdfasdf ads', 'sd fasdf asdfasdf ads', 4),
(15, 'bronze', 'sd fasdf asdfasdf ads', 'sd fasdf asdfasdf ads', 4),
(16, 'gold', 'sd fasdf asdfasdf ads1111', 'sd fasdf asdfasdf ads1111', 5),
(17, 'silver', 'sd fasdf asdfasdf ads1111', 'sd fasdf asdfasdf ads1111', 5),
(18, 'bronze', 'sd fasdf asdfasdf ads1111', 'sd fasdf asdfasdf ads1111', 5);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_winners_competition`
--

CREATE TABLE `t_kg_winners_competition` (
`id` int(11) NOT NULL,
  `years` varchar(16) NOT NULL COMMENT 'Год',
  `type_id` int(11) NOT NULL COMMENT 'Тип конкурса',
  `name` varchar(512) NOT NULL COMMENT 'Название',
  `description` text NOT NULL COMMENT 'Описание',
  `position` int(11) NOT NULL DEFAULT '99' COMMENT 'Позиция отображения'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица конкурс' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `t_kg_winners_competition`
--

INSERT INTO `t_kg_winners_competition` (`id`, `years`, `type_id`, `name`, `description`, `position`) VALUES
(1, '2013', 1, 'Мультибрендовые и мультикатегорийные компании', 'Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России, Брэнд года стал наиболее значимой наградой для компаний, нацеленных на лидерство на рынке независимо от отрасли. ', 1),
(2, '2013', 1, 'Продукты питания', 'Продукты питания Запущенный в 1998 году с целью продвижения современных инструментов рыночной деятельности в России.', 2),
(3, '2013', 2, 'Отдых, развлечения, туризм, культура, спорт,образование', 'Награда Effie вручается за главное достижение в сфере рекламы и маркетинговых коммуникации — эффективность.', 1),
(4, '2023', 2, 'sd fasdf asdfasdf ads', 'sd fasdf asdfasdf ads', 2),
(5, '2222', 2, 'sd fasdf asdfasdf ads1111', 'sd fasdf asdfasdf ads1111', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_winners_competition_type`
--

CREATE TABLE `t_kg_winners_competition_type` (
`id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL COMMENT 'Тип конкурса'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t_kg_winners_competition_type`
--

INSERT INTO `t_kg_winners_competition_type` (`id`, `name`) VALUES
(1, 'Основной конкурс'),
(2, 'Специальный конкурс');

-- --------------------------------------------------------

--
-- Table structure for table `t_kg_winners_years`
--

CREATE TABLE `t_kg_winners_years` (
`id` int(11) NOT NULL,
  `year` varchar(16) NOT NULL COMMENT 'Год'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Список года ' AUTO_INCREMENT=11 ;

--
-- Dumping data for table `t_kg_winners_years`
--

INSERT INTO `t_kg_winners_years` (`id`, `year`) VALUES
(1, '2013'),
(2, '2012'),
(3, '2011'),
(4, '2010'),
(5, '2009'),
(6, '2008'),
(9, '2023'),
(10, '2222');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `frontend_session`
--
ALTER TABLE `frontend_session`
 ADD PRIMARY KEY (`id`), ADD KEY `expire` (`expire`);

--
-- Indexes for table `t_kg_comment`
--
ALTER TABLE `t_kg_comment`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_competition_stage`
--
ALTER TABLE `t_kg_competition_stage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_content_block`
--
ALTER TABLE `t_kg_content_block`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_draft`
--
ALTER TABLE `t_kg_draft`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_file`
--
ALTER TABLE `t_kg_file`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_gallery`
--
ALTER TABLE `t_kg_gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_jury`
--
ALTER TABLE `t_kg_jury`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_mail_template`
--
ALTER TABLE `t_kg_mail_template`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_menu`
--
ALTER TABLE `t_kg_menu`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_menu_group`
--
ALTER TABLE `t_kg_menu_group`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_migration`
--
ALTER TABLE `t_kg_migration`
 ADD PRIMARY KEY (`version`);

--
-- Indexes for table `t_kg_module`
--
ALTER TABLE `t_kg_module`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_nomination`
--
ALTER TABLE `t_kg_nomination`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_nomination_category`
--
ALTER TABLE `t_kg_nomination_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_pages`
--
ALTER TABLE `t_kg_pages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_participant_of_competition`
--
ALTER TABLE `t_kg_participant_of_competition`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_partners`
--
ALTER TABLE `t_kg_partners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_partners_category`
--
ALTER TABLE `t_kg_partners_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_photo`
--
ALTER TABLE `t_kg_photo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_photogallery`
--
ALTER TABLE `t_kg_photogallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_post`
--
ALTER TABLE `t_kg_post`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_user`
--
ALTER TABLE `t_kg_user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_user_role`
--
ALTER TABLE `t_kg_user_role`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_winners`
--
ALTER TABLE `t_kg_winners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_winners_competition`
--
ALTER TABLE `t_kg_winners_competition`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_winners_competition_type`
--
ALTER TABLE `t_kg_winners_competition_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kg_winners_years`
--
ALTER TABLE `t_kg_winners_years`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_kg_comment`
--
ALTER TABLE `t_kg_comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_kg_competition_stage`
--
ALTER TABLE `t_kg_competition_stage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_kg_content_block`
--
ALTER TABLE `t_kg_content_block`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `t_kg_draft`
--
ALTER TABLE `t_kg_draft`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_kg_file`
--
ALTER TABLE `t_kg_file`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `t_kg_gallery`
--
ALTER TABLE `t_kg_gallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_kg_jury`
--
ALTER TABLE `t_kg_jury`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_kg_mail_template`
--
ALTER TABLE `t_kg_mail_template`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_kg_menu`
--
ALTER TABLE `t_kg_menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_kg_menu_group`
--
ALTER TABLE `t_kg_menu_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_kg_module`
--
ALTER TABLE `t_kg_module`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_kg_nomination`
--
ALTER TABLE `t_kg_nomination`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `t_kg_nomination_category`
--
ALTER TABLE `t_kg_nomination_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_kg_pages`
--
ALTER TABLE `t_kg_pages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_kg_participant_of_competition`
--
ALTER TABLE `t_kg_participant_of_competition`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `t_kg_partners`
--
ALTER TABLE `t_kg_partners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_kg_partners_category`
--
ALTER TABLE `t_kg_partners_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_kg_photo`
--
ALTER TABLE `t_kg_photo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `t_kg_photogallery`
--
ALTER TABLE `t_kg_photogallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `t_kg_post`
--
ALTER TABLE `t_kg_post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_kg_user`
--
ALTER TABLE `t_kg_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `t_kg_user_role`
--
ALTER TABLE `t_kg_user_role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_kg_winners`
--
ALTER TABLE `t_kg_winners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `t_kg_winners_competition`
--
ALTER TABLE `t_kg_winners_competition`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_kg_winners_competition_type`
--
ALTER TABLE `t_kg_winners_competition_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_kg_winners_years`
--
ALTER TABLE `t_kg_winners_years`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
