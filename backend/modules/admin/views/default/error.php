<?php

use yii\helpers\Html;

$this->registerAssetBundle('backend\modules\post\assets\PostModuleAsset', \yii\web\View::POS_HEAD);

/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */


$user = \common\modules\user\models\User::findIdentity(Yii::$app->user->getId());


$this->title = $name;
?>
<div class="container" style="margin-top: 50px;">
    <div class="site-error">

        <?php
        if ($user->role->id == 2) {
            $this->registerMetaTag(['http-equiv' => 'refresh', 'content' => '5;URL="/"']);
            ?>
            <h1>Добро пожаловать модератор - <?= Html::encode($user->username) ?></h1>
            <div class="alert alert-info">
                <p style="font-size: 16px;">Для того, чтобы продолжить редактирование сайта необходимо пройти по <a href="/">ссылке</a> или подождать 5 секунд.</p>
            </div>
        <?
        } else {
            ?>
            <h1><?= Html::encode($this->title) ?></h1>

            <div class="alert alert-danger">
                <?= nl2br(Html::encode($message)) ?>
            </div>

<!--            <p>-->
<!--                The above error occurred while the Web server was processing your request.-->
<!--            </p>-->
<!---->
<!--            <p>-->
<!--                Please contact us if you think this is a server error. Thank you.-->
<!--            </p>-->
        <?php
        }
        ?>


    </div>
</div>