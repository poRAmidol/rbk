<?php

use yii\helpers\Html;


$this->registerAssetBundle('backend\assets\FileUploadAsset', \yii\web\View::POS_HEAD);

/* @var $this yii\web\View */
/* @var $model common\modules\gallery\models\Photogallery */

$this->title = 'Создать фотогалерею';
$this->params['breadcrumbs'][] = ['label' => 'Фотогалерея', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photogallery-create padding020 widget">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'photos' => $photos
    ]) ?>

</div>
