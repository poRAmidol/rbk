<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\modules\gallery\models\Photogallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photogallery-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'fileupload',
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?=$form->errorSummary($model);?>

    <?= $form->errorSummary([$model, $photos], ['class' => 'alert alert-danger']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'description', [
        'template' => "
                {label}
                <div style='color:#000;'>
                {input}
                </div>
                {error}
            "
    ])->widget(sim2github\imperavi\widgets\Redactor::className(), [
        'options' => [
            'debug' => 'true',
        ],
        'clientOptions' => [ // [More about settings](http://imperavi.com/redactor/docs/settings/)
            'convertImageLinks' => 'false', //By default
            'convertVideoLinks' => 'false', //By default
            'buttonSource' => true,
            //'wym' => 'true',
            //'air' => 'true',
            'linkEmail' => 'true', //By default
            'lang' => 'ru',
            'tidyHtml' => true,
            'paragraphize' => false,
            'allowedTags' => ['p', 'blockquote', 'b', 'strong', 'i', 'ul', 'li', 'ol', 'a', 'div', 'span', 'bold', 'table', 'tr', 'td', 'thead', 'tbody', 'tfoot', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
            'phpTags' => true,
            'pastePlainText' => false,
            'replaceDivs' => false,
            'convertDivs' => false,
            'deniedTags' => false,
//            'imageGetJson' =>  \Yii::getAlias('@web').'/redactor/upload/imagejson', //By default
            'plugins' => [ // [More about plugins](http://imperavi.com/redactor/plugins/)
                'ace',
                'clips',
//                'fullscreen'
            ]
        ],

    ])
    ?>

    <div id="photos" class="tab-pane">
        <?
        if (!empty($model->photos)) {
            echo '<ul class="photos-list">';
            foreach ($model->photos as $photo) {
                echo "<li>
                    <p>" . Html::a("<span class='glyphicon glyphicon-trash'></span>", ['/gallery/default/photo-delete', 'id' => $photo->id], ['class' => 'deletePhoto']) . "&nbsp;&nbsp;&nbsp;
                       " . Html::a("<span class='glyphicon glyphicon-repeat'></span>", ['/gallery/default/ajax-rotate', 'id' => $photo->id], ['class' => 'rotatePhoto']) . "&nbsp;&nbsp;&nbsp;
                       " . Html::a("<span class='glyphicon " . ($photo->is_main == 0 ? "glyphicon-unchecked" : "glyphicon-check") . "'></span>", ['/gallery/default/ajax-change-main-status', 'id' => $photo->id], ['class' => 'ajaxChangeMainStatus']) . "&nbsp;&nbsp;&nbsp;
                    </p>" . Html::img("/" . $photo->doCache('200x200', 'auto')) . "
               </li>";
            }
            echo '</ul>';
        }

        $photo = new \common\modules\gallery\models\Photo();
        ?>
        <?=$form->field($photo, 'name[]')->fileInput(['multiple' => true]); ?>
    </div>


    <filedset>
        <legend>SEO Атрибутика</legend>
        <?= $form->field($model, 'seo_title')->textInput(['maxlength' => 512]) ?>

        <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 512]) ?>

        <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>
    </filedset>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script id="template-upload" type="text/template">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
        <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
        <td>
            <!--<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0"-->
            <!--aria-valuemax="100" aria-valuenow="0">-->
            <!--<div class="bar" style="width:0%;"></div>-->
            <!--</div>-->
        </td>
        <td>{% if (!o.options.autoUpload) { %}
            <!--<button class="btn btn-primary btn-sm start">-->
            <!--<i class="fa fa-upload"></i>-->
            <!--<span>Start</span>-->
            <!--</button>-->
            {% } %}
        </td>
        {% } else { %}
        <td colspan="2"></td>
        {% } %}
        <td>{% if (!i) { %}
            <button class="btn btn-warning btn-sm cancel">
                <i class="fa fa-ban"></i>
                <span>Отмена</span>
            </button>
            {% } %}
        </td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/template">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
        <td></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else { %}
        <td class="preview">{% if (file.thumbnail_url) { %}
            <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img
                    src="{%=file.thumbnail_url%}"></a>
            {% } %}
        </td>
        <td class="name">
            <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}"
               download="{%=file.name%}">{%=file.name%}</a>
        </td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td colspan="2"></td>
        {% } %}
        <td>
            <button class="btn btn-danger btn-sm delete" data-type="{%=file.delete_type%}"
                    data-url="{%=file.delete_url%}"
                    {% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}' {% } %}>
                <i class="fa fa-trash"></i>
                <span>Delete</span>
            </button>
        </td>
    </tr>
    {% } %}
</script>

<script type="text/javascript">

    $(document).ready(function () {

//        setTimeout(checkFormError());

        $('.ajaxChangeMainStatus').on('click', function () {
            var _url = $(this).attr('href');
            var element = $(this);
            $.ajax({
                type: 'post',
                url: _url,
                dataType: 'json',
                success: function (json) {

//                    "glyphicon-unchecked" : "glyphicon-check"
                    if (json.error) {
                        alert(json.message);
                    } else {
//                        $('.photos-list li p a.ajaxChangeMainStatus').each(function (i) {

//                        });
                        if (json.is_main == 1) {
                            element.children('span').removeClass('glyphicon-unchecked');
                            element.children('span').addClass('glyphicon-check');
                        } else {
                            element.children('span').removeClass('glyphicon-check');
                            element.children('span').addClass('glyphicon-unchecked');
                        }

                    }

                }
            });
            return false;
        });

        $('.deletePhoto').on('click', function () {
            if (confirm('Вы действительно хотите удалить фото?')) {
                var _Url = $(this).attr('href');
                var _li = $(this).parent('p').parent('li');

                $.ajax({
                    type: 'POST',
                    url: _Url,
                    dataType: 'json',
                    success: function (msg) {
                        if (msg.error) {
                            alert(msg.message);
                        } else {
                            _li.hide('slow');
                        }
                    }
                });
            }
            return false;
        });

        $('.rotatePhoto').on('click', function () {
            var _url = $(this).attr('href');
            var _img = $(this).parent('p').parent('li').children('img');

            $.ajax({
                type: 'POST',
                url: _url,
                dataType: 'json',
                success: function (msg) {
                    if (msg.error) {
                        alert(msg.message);
                    } else {
                        $(_img).attr('src', $(_img).attr('src') + '?' + Math.random());
                    }
                }
            });
            return false;
        });
    });


</script>