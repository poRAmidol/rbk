<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\gallery\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>


    <div class="row">
        <div class="col-lg-5">
            <?= $form->field($model, 'is_main')->checkbox(['maxlength' => 512]) ?>
        </div>
        <div class="col-lg-6"></div>
    </div>
    <div class="row">
        <div class="col-lg-5"><?= $form->field($model, 'img')->fileInput(); ?></div>
        <div class="col-lg-6">
            <?php
            if ($model->img != "") {
                echo Html::tag('div',
                    Html::tag('div',
                        Html::img("/" . $model->doCache('400x300', 'auto', '400x300')) . Html::a(Html::tag('i', '', ['class' => 'fa fa-times-circle-o']) . " " . 'Удалить', ['/gallery/gallery/remove-img', 'id' => $model->id],
                            [
                                'class' => 'btn btn-danger',
                            ]), ['class' => 'photo-item']
                    ), ['class' => 'show-img']
                );
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <?= $form->field($model, 'video')->textarea(['rows' => 5]) ?>
        </div>
        <div class="col-lg-6">
            <?php
            if ($model->video != "") {
                ?>
                <iframe width="560" height="315" src="<?= $model->video ?>" frameborder="0" allowfullscreen></iframe>
            <?php
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <?= $form->field($model, 'position')->textInput(['maxlength' => 512]) ?>
        </div>
        <div class="col-lg-6"></div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
