<?php

namespace backend\modules\gallery\controllers;

use common\modules\gallery\models\Photo;
use common\modules\user\models\User;
use Yii;
use common\modules\gallery\models\Photogallery;
use common\modules\gallery\models\search\PhotogallerySearch;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DefaultController implements the CRUD actions for Photogallery model.
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post', 'get'],
                ],

            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'ajax-change-main-status', 'ajax-rotate', 'photo-delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'ajax-change-main-status', 'ajax-rotate', 'photo-delete'],
                        'matchCallback' => function ($rule, $action) {
                            $model = User::findIdentity(Yii::$app->user->getId());
                            if (!empty($model)) {
                                return $model->role->id == 1; // Администратор
                            }
                            return false;
                        }
                    ]
                ]
            ]
        ];
    }

    public function actionAjaxChangeMainStatus($id) {
        if (Yii::$app->request->isAjax) {
            $model = Photo::findOne((int)$id);
            if (!empty($model)) {
//                Photo::updateAll(['is_main' => 0]);
                $model->is_main = ($model->is_main == 1 ? 0 : 1);
                if ($model->save()) {
                    return json_encode(['error' => false, 'is_main' => $model->is_main]);
                } else {
                    return json_encode(['error' => true, 'message' => $model->getErrors()]);
                }
            }
            return false;
        }
    }

    public function actionAjaxRotate($id) {
        if (Yii::$app->request->isAjax) {
            $photo = Photo::findOne((int)$id);

            if (!empty($photo)) {
                //FileHelper::removeDirectory(Yii::$app->basePath."/../".$this->src);

                FileHelper::removeDirectory(Yii::$app->basePath."/../cache/".$photo->src);

                $img = Yii::$app->image->load(Yii::$app->basePath."/../".$photo->src . "/" . $photo->name);

                if ($img->rotate(90)->save(Yii::$app->basePath."/../".$photo->src . "/" . $photo->name)) {
                    $photo->doCache('200x200');
                    return json_encode(array('error' => false));

                } else {
                    return json_encode(array('error' => true, 'message' => 'Фото неудалено'));

                }
            }
            return json_encode(array('error' => true, 'message' => 'Фото не найдено'));

        }

        /**/
    }
    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionPhotoDelete($id)
    {
        if (Yii::$app->request->isAjax) {
            $model = Photo::findOne((int) $id);
            if (!empty($model)) {
                if ($model->delete()) {
                    return json_encode(array('error' => false));
                } else {
                    return json_encode(array('error' => true, 'message' => $model->getErrors()));
                }
            }
        }

    }

    /**
     * Lists all Photogallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PhotogallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Photogallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Photogallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Photogallery();
        $photos = new Photo();
        $temp_photos = null;
        $_temp_files = null;
        $validate = true;

        if (isset($_POST['Photogallery'])) {

            // проверям валидность загружаемых файлов
            $photosArr = UploadedFile::getInstances($photos, 'name');
            if (!empty($photosArr))
                $photos->name = $photosArr;

            // Данные о продукте
            $model->load(Yii::$app->request->post());
            if (!empty($photos->name))
                $validate = $validate && ($photos->name && $photos->validate());

            $validate = $validate && $model->validate();


            // Сохраняем модель Product
            if ($validate && $model->save()) {

                // Загружаем файлы и сохраняем Photo
                $photos->uploadAndSavePhoto($model);


                if (Yii::$app->request->post('submit') == 'save-and-continue') {
                    return $this->redirect(['create']);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }


        return $this->render('create', [
            'model' => $model,
            'photos' => $photos
        ]);
    }

    /**
     * Updates an existing Photogallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $photos = new Photo();
        $temp_photos = null;
        $validate = true;

        if (isset($_POST['Photogallery'])) {
            $photosArr = UploadedFile::getInstances($photos, 'name');
            if (!empty($photosArr))
                $photos->name = $photosArr;

            // Данные о продукте
            $model->load(Yii::$app->request->post());
            if (!empty($photos->name))
                $validate = $validate && ($photos->name && $photos->validate());

            $validate = $validate && $model->validate();

            // Сохраняем модель Product
            if ($validate && $model->save()) {

                // Загружаем файлы и сохраняем Photo
                $photos->uploadAndSavePhoto($model);

                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('update', [
            'model' => $model,
            'photos' => $photos
        ]);
    }

    /**
     * Deletes an existing Photogallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Photogallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Photogallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Photogallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
