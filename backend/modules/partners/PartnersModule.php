<?php

namespace backend\modules\partners;

class PartnersModule extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\partners\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }


}
