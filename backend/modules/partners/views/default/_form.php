<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\partners\models\Partners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partners-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'fileupload',
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->errorSummary([$model], ['class' => 'alert alert-danger']); ?>
    <div class="row">
        <div class="col-lg-9">
            <?php
            if ($model->logo != "") {
                echo Html::tag('div',
                    Html::tag('div',
                        Html::img("/".$model->logo_src."/".$model->logo) . Html::a(Html::tag('i', '', ['class' => 'fa fa-times-circle-o']) . " " . 'Удалить', ['/partners/default/remove-img', 'id' => $model->id],
                            [
                                'class' => 'btn btn-danger',
//                            'data' => [
//                                'confirm' => 'Вы действительно хотите удалить фото?',
//                                'method' => 'get',
//                            ]
                            ]), ['class' => 'photo-item']
                    ), ['class' => 'show-img']
                );
            } else {
                echo $form->field($model, 'logo')->fileInput();
            }
            ?>

            <div class="row">
                <div class="col-lg-4">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => 512]) ?>
                </div>
                <div class="col-lg-4">
                    <?= $form->field($model, 'url')->textInput() ?>
                </div>
                <div class="col-lg-4">
                    <?= $form->field($model, 'position')->textInput() ?>
                </div>
            </div>



            <?= $form->field($model, 'description', [
                'template' => "
                {label}
                <div style='color:#000;'>
                {input}
                </div>
                {error}
            "
            ])->widget(sim2github\imperavi\widgets\Redactor::className(), [
                'options' => [
                    'debug' => 'true',
                ],
                'clientOptions' => [ // [More about settings](http://imperavi.com/redactor/docs/settings/)
                    'convertImageLinks' => 'false', //By default
                    'convertVideoLinks' => 'false', //By default
                    'buttonSource' => true,
                    //'wym' => 'true',
                    //'air' => 'true',
                    'linkEmail' => 'true', //By default
                    'lang' => 'ru',
                    'tidyHtml' => true,
                    'paragraphize' => false,
                    'allowedTags' => ['p', 'blockquote', 'b', 'strong', 'i', 'ul', 'li', 'ol', 'a', 'div', 'span', 'bold', 'table', 'tr', 'td', 'thead', 'tbody', 'tfoot', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
                    'phpTags' => true,
                    'pastePlainText' => false,
                    'replaceDivs' => false,
                    'convertDivs' => false,
                    'deniedTags' => false,
//            'imageGetJson' =>  \Yii::getAlias('@web').'/redactor/upload/imagejson', //By default
                    'plugins' => [ // [More about plugins](http://imperavi.com/redactor/plugins/)
                        'ace',
                        'clips',
//                        'fullscreen'
                    ]
                ],

            ])
            ?>
        </div>
        <div class="col-lg-3">
            <p>Категория</p>
            <?php
            $categories = \common\modules\partners\models\PartnersCategory::find()->orderBy('position ASC')->all();
            echo Html::checkboxList('categories', $model->categoriesArr, \yii\helpers\ArrayHelper::map($categories, 'id', 'name'));
            ?>
        </div>
    </div>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
