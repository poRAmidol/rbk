<?php

namespace backend\modules\participant\controllers;

use backend\modules\participant\models\EmailSenderForm;
use common\modules\user\models\User;
use Yii;
use common\modules\participant\models\ParticipantOfCompetition;
use common\modules\participant\models\search\ParticipantOfCompetitionSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for ParticipantOfCompetition model.
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post', 'get'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'email'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'email'],
//                        'roles' => ['@']
                        'matchCallback' => function ($rule, $action) {
                            $model = User::findIdentity(Yii::$app->user->getId());
                            if (!empty($model)) {
                                return $model->role->id == 1; // Администратор
                            }
                            return false;
                        }
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all ParticipantOfCompetition models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParticipantOfCompetitionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ParticipantOfCompetition model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ParticipantOfCompetition model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ParticipantOfCompetition();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ParticipantOfCompetition model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ParticipantOfCompetition model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ParticipantOfCompetition model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParticipantOfCompetition the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParticipantOfCompetition::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEmail()
    {
        $model = new EmailSenderForm();
        $success = 0;
        $fail = 0;
        $total = 0;
        if ($model->load(Yii::$app->request->post())) {
            if (!empty($model->brands)) {
                $total = count($model->brands);
                foreach ($model->brands as $brand) {
                    $u = ParticipantOfCompetition::findOne((int) $brand);
                    $result = \Yii::$app->mailer->compose('@common/mail/simple-email', ['body' => $model->text])
                        ->setFrom(\Yii::$app->params['rbkEmail'])
                        ->setTo($u->email)
                        ->setSubject($model->subject)
                        ->send();
                    if ($result) {
                        $success = $success + 1;
                    } else {
                        $fail = $fail - 1;
                    }

                }
                if ($success) {
                    \Yii::$app->getSession()->setFlash('success', 'Сообщения отправлены '.$success.' из '.$total);
                }
                if ($fail) {
                    \Yii::$app->getSession()->setFlash('error', 'Сообщения нет отправлены '.$fail.' из '.$total);
                }
            }
        }

        return $this->render('email', ['model' => $model]);
    }
}
