<?php

use yii\helpers\Html;

$this->registerAssetBundle('backend\modules\layoutEditor\assets\AceEditorAsset', \yii\web\View::POS_HEAD);

/* @var $this yii\web\View */
/* @var $model common\modules\participant\models\ParticipantOfCompetition */

$this->title = 'Create Participant Of Competition';
$this->params['breadcrumbs'][] = ['label' => 'Participant Of Competitions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participant-of-competition-create padding020 widget">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
