<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->registerAssetBundle('backend\modules\layoutEditor\assets\AceEditorAsset', \yii\web\View::POS_HEAD);

/* @var $this yii\web\View */
/* @var $searchModel common\modules\participant\models\search\ParticipantOfCompetitionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявка на участи в конкурсе';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participant-of-competition-index padding020 widget">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Рассылка', ['email'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'brand_name',
            [
                'attribute' => 'nomination_id',
                'value' => function ($model, $action) {
                    return (empty($model->nomination) ? "---" : $model->nomination->name);
                },
                'filter' => \yii\helpers\ArrayHelper::map(\common\modules\nomination\models\Nomination::find()->all(), 'id', 'name')
            ],
            [
                'attribute' => 'seminar',
                'value' => function ($model, $action) {
                    return $model->seminar == 1 ? "Да" : "Нет";
                },
                'filter' => ["Нет", "Да"]
            ],
            [
                'attribute' => 'status',
                'value' => function($model, $action) {
                    return $model->statusArr[$model->status];
                },
                'filter' => [0 => "Не рассмотрена", 1 => "Принята", 2 => "Отклонена"]
            ],
            'first_name',
            'last_name',
            // 'last_name',
            // 'job',
            // 'phone',
            // 'email:email',
            // 'reg_date',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
