<?php

use yii\helpers\Html;
use common\modules\participant\models\ParticipantOfCompetition;
use yii\widgets\ActiveForm;

$this->registerJs('

    $(".checkall").on("click", function(){
        $(".brands input:checkbox").not(this).prop("checked", this.checked);
    });

', \yii\web\View::POS_READY);

$this->registerAssetBundle('backend\modules\layoutEditor\assets\AceEditorAsset', \yii\web\View::POS_HEAD);

/* @var $this yii\web\View */
/* @var $model common\modules\participant\models\ParticipantOfCompetition */

$this->title = 'Рассылка';
$this->params['breadcrumbs'][] = ['label' => 'Заявки на участие в конкурсе', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participant-of-competition-create padding020 widget">
    <?php
    if (Yii::$app->getSession()->hasFlash('success')) {
        echo "<div class='badge-success' style='padding: 10px 18px 10px 18px;'>".Yii::$app->getSession()->getFlash('success')."</div>";
    }

    if (Yii::$app->getSession()->hasFlash('error')) {
        echo "<div class='badge-warning' style='padding: 10px 18px 10px 18px;'>".Yii::$app->getSession()->getFlash('error')."</div>";
    }

    ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-8">
            <?=$form->field($model, 'subject')->textInput()?>
            <?= $form->field($model, 'text', [
                'template' => "
                {label}
                <div style='color:#000;'>
                {input}
                </div>
                {error}
            "
            ])->widget(sim2github\imperavi\widgets\Redactor::className(), [
                'options' => [
                    'debug' => 'true',
                ],
                'clientOptions' => [ // [More about settings](http://imperavi.com/redactor/docs/settings/)
                    'convertImageLinks' => 'false', //By default
                    'convertVideoLinks' => 'false', //By default
                    'buttonSource' => true,
                    //'wym' => 'true',
                    //'air' => 'true',
                    'linkEmail' => 'true', //By default
                    'lang' => 'ru',
                    'tidyHtml' => true,
                    'paragraphize' => false,
                    'allowedTags' => ['p', 'blockquote', 'b', 'strong', 'i', 'ul', 'li', 'ol', 'a', 'div', 'span', 'bold', 'table', 'tr', 'td', 'thead', 'tbody', 'tfoot', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
                    'phpTags' => true,
                    'pastePlainText' => false,
                    'replaceDivs' => false,
                    'convertDivs' => false,
                    'deniedTags' => false,
//            'imageGetJson' =>  \Yii::getAlias('@web').'/redactor/upload/imagejson', //By default
                    'plugins' => [ // [More about plugins](http://imperavi.com/redactor/plugins/)
                        'ace',
                        'clips',
                        'fullscreen'
                    ]
                ],

            ])
            ?>
        </div>
        <div class="col-lg-4" class="brands">
            <?=$form->field($model, 'brands',[
                'template' => '
                    <div>{label}</div>
                    <label><input type="checkbox" name="all" class="checkall" /> Выбрать все</label>
                    <div class="brands">{input}</div>
                    <div>{error}</div>
                '
            ])->checkboxList(\yii\helpers\ArrayHelper::map(ParticipantOfCompetition::find()->all(), "id", "brand_name"))?>
        </div>
    </div>


    <div class="form-actions">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
