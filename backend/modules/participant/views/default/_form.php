<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\participant\models\ParticipantOfCompetition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participant-of-competition-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'status')->dropDownList($model->statusArr) ?>
        </div>
    </div>

    <?= $form->field($model, 'seminar')->checkbox() ?>



    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'brand_name')->textInput(['maxlength' => 512]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'nomination_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\modules\nomination\models\Nomination::find()->all(), "id", "name"), ['prompt' => '---']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => 512]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => 512]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'job')->textInput(['maxlength' => 512]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => 512]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => 512]) ?>
        </div>
    </div>














    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
