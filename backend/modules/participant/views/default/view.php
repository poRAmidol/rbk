<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->registerAssetBundle('backend\modules\layoutEditor\assets\AceEditorAsset', \yii\web\View::POS_HEAD);

/* @var $this yii\web\View */
/* @var $model common\modules\participant\models\ParticipantOfCompetition */

$this->title = $model->brand_name;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на участи в конкурсе', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participant-of-competition-view padding020 widget">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Список', ['update', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'brand_name',

            [
                'attribute' => 'nomination_id',
                'value' => (empty($model->nomination) ? "---" : $model->nomination->name),
            ],
            [
                'attribute' => 'seminar',
                'value' => ($model->seminar == 1 ? "Да" : "Нет"),
            ],

            'first_name',
            'last_name',
            'job',
            'phone',
            'email:email',
            'reg_date:date',
            [
                'attribute' => 'status',
                'value' => $model->statusArr[$model->status],
            ]
        ],
    ]) ?>

</div>
