<?php
/**
 * Created by PhpStorm.
 * User: pashaevs
 * Date: 22.01.15
 * Time: 21:23
 */

namespace backend\modules\participant\models;

class EmailSenderForm extends \yii\base\Model {

    public $subject;
    public $text;

    public $brands;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'text'], 'required'],
            [['subject'], 'string', 'max' => 512],
            [['text'], 'string'],
            [['brands'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subject' => 'Тема',
            'text' => 'Тело письма',
            'brands' => 'Бренды'
        ];
    }

}
