<?php

namespace backend\modules\mailer;

class MailerModule extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\mailer\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
