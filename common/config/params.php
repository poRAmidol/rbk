<?php
return [
    'adminEmail' => 'admin@kagama.ru',
    'rbkEmail' => 'effie@rbc.ru',
    'supportEmail' => 'support@rbc.ru',
    'user.passwordResetTokenExpire' => 3600,
];
