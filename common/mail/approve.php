<h1>Уважаемый <?=$participant->first_name?> <?=$participant->last_name?>,</h1>
<p>Поданная Вами заявка на участие в конкурсе EFFIE/ Брэнд года в номинации <?=$participant->nomination->name?> для бренда <?=$participant->brand_name?> принята.
    Наши представители свяжутся с Вами для уточнения деталей.</p>