<?php

namespace common\modules\partners;

class PartnerModule extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\partners\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
