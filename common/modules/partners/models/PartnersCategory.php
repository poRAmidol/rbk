<?php

namespace common\modules\partners\models;

use Yii;

/**
 * This is the model class for table "{{%partners_category}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $position
 */
class PartnersCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partners_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['position'], 'integer'],
            [['name'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'position' => 'Позиция отображения'
        ];
    }

//    public function getPartners()
//    {
//        return $this->hasMany(PartnersCategoryRelation::className(), ['category_id' => 'id']);
//    }

    public function getPRelations()
    {
        return $this->hasMany(PartnersCategoryRelation::className(), ['category_id' => 'id']);
    }


    public function getPartners()
    {
        return $this->hasMany(Partners::className(), ['id' => 'partner_id'])->orderBy('position ASC')
            ->via('pRelations');
    }
}
