<?php

namespace common\modules\partners\models;

use common\helpers\CDirectory;
use common\helpers\CString;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%partners}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $logo
 * @property string $logo_src
 * @property string $description
 * @property integer $position
 * @property string $url
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partners}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['position'], 'integer'],
            [['position'], 'default', 'value' => 99],
            [['name', 'logo_src', 'url'], 'string', 'max' => 512],
            [['url'], 'url'],
            [['logo'], 'image', 'extensions' => 'jpg, gif, jpeg, png', 'skipOnEmpty' => true],
            [['logo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'logo' => 'Логотип',
            'logo_src' => 'Путь к лого',
            'description' => 'Описание',
            'position' => 'Позиция отображения',
            'url' => 'Ссылка на сайт компании'
        ];
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\CachedImageResolution',
                'attr_src' => 'logo_src',
                'attr_img_name' => 'logo',
            ]
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $logo = UploadedFile::getInstance($this, 'logo');
            if ($logo instanceof UploadedFile && $logo->size > 0) {

                $this->logo = $logo;

                $path = 'images/modules/partners/' . CString::translitTo($this->name);
                CDirectory::createDir($path);
                $dir = Yii::$app->basePath . "/../" . $path;

                $imageName = md5(CString::translitTo($this->logo->name) . microtime()) . "." . $this->logo->getExtension();
                $this->logo->saveAs($dir . "/" . $imageName);

                $this->logo = $imageName;
                $this->logo_src = $path;

            }
            $this->description = CString::typography($this->description);

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $attr)
    {
//        if (parent::afterSave($insert, $attr)) {
        $catArr = Yii::$app->request->post('categories', null);
        if ($catArr != null) {
            $prym_key = $this->getPrimaryKey();
            PartnersCategoryRelation::deleteAll(['partner_id' => $prym_key]);

            foreach ($catArr as $cat_id) {
                $rel = new PartnersCategoryRelation();
                $rel->partner_id = $prym_key;
                $rel->category_id = $cat_id;
                $rel->save();
            }

        }
        return true;
//        }

//        return false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deletePhoto();
            PartnersCategoryRelation::deleteAll(['partner_id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }

    public function deletePhoto()
    {
        if (is_file(Yii::$app->basePath . "/../" . $this->logo_src . "/" . $this->logo)) {
            unlink(Yii::$app->basePath . "/../" . $this->logo_src . "/" . $this->logo);
            FileHelper::removeDirectory(Yii::$app->basePath . "/../cache/" . $this->logo_src);
        }
    }

    public function getCategoriesArr()
    {
        $relArr = PartnersCategoryRelation::find()->where(['partner_id' => $this->getPrimaryKey()])->all();
        if ($relArr) {
            $catArr = [];
            foreach ($relArr as $cat) {
                $catArr[] = $cat->category_id;
            }
            return $catArr;
        }
        return null;
    }

    public static function findAllByCategoryId($category_id)
    {
        $relsArr = PartnersCategoryRelation::find()->where(['category_id' => $category_id])->all();
        if (empty($relsArr)) {
            return null;
        } else {
            $prodsId = [];
            foreach ($relsArr as $rel) {
                $prodsId[] = $rel->partner_id;
            }
            return self::find()->where('id in ('.implode(', ',$prodsId).") ")->all();
        }

    }
}
