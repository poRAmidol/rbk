<?php

namespace common\modules\partners\models;

use Yii;

/**
 * This is the model class for table "{{%partners_category_relation}}".
 *
 * @property integer $partner_id
 * @property integer $category_id
 */
class PartnersCategoryRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partners_category_relation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'category_id'], 'required'],
            [['partner_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'partner_id' => 'Партнер',
            'category_id' => 'Категория',
        ];
    }

    public function getPartners($category_id)
    {
        return $this->hasMany(Partners::className(), ['id' => 'partner_id'])->where(['category_id' => $category_id])->orderBy('position ASC');
    }
}
