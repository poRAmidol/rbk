<?php

namespace common\modules\mailer\models;

use Yii;

/**
 * This is the model class for table "{{%mail_template}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $subject
 * @property string $mail_body
 */
class MailTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mail_template}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'subject', 'mail_body'], 'required'],
            [['mail_body'], 'string'],
            [['name', 'subject'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название шаблона',
            'subject' => 'Заголовок',
            'mail_body' => 'Тело письма',
        ];
    }
}
