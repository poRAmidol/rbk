<?php

namespace common\modules\mailer;

class MailerModule extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\mailer\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
