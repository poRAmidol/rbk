<?php

namespace common\modules\participant\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\participant\models\ParticipantOfCompetition;

/**
 * ParticipantOfCompetitionSearch represents the model behind the search form about `common\modules\participant\models\ParticipantOfCompetition`.
 */
class ParticipantOfCompetitionSearch extends ParticipantOfCompetition
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nomination_id', 'seminar', 'reg_date', 'status'], 'integer'],
            [['brand_name', 'first_name', 'last_name', 'job', 'phone', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ParticipantOfCompetition::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'nomination_id' => $this->nomination_id,
            'seminar' => $this->seminar,
            'reg_date' => $this->reg_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'brand_name', $this->brand_name])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'job', $this->job])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
