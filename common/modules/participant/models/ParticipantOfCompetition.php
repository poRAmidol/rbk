<?php

namespace common\modules\participant\models;

use common\modules\nomination\models\Nomination;
use Yii;

/**
 * This is the model class for table "{{%participant_of_competition}}".
 *
 * @property integer $id
 * @property string $brand_name
 * @property integer $nomination_id
 * @property integer $seminar
 * @property string $first_name
 * @property string $last_name
 * @property string $job
 * @property string $phone
 * @property string $email
 * @property integer $reg_date
 * @property integer $status
 */
class ParticipantOfCompetition extends \yii\db\ActiveRecord
{

    public $statusArr = [0 => "Не рассмотрена", 1 => "Принята", 2 => "Отклонена"];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%participant_of_competition}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['brand_name', 'company_name', 'nomination_id', 'seminar', 'first_name', 'last_name', 'job', 'phone', 'email'], 'required'],
            [['nomination_id', 'seminar', 'reg_date', 'status'], 'integer'],
            [['brand_name', 'first_name', 'last_name', 'job', 'phone', 'email'], 'string', 'max' => 512],
            [['phone'], 'match', 'pattern' => '/^(\+7|8)\s?\((\d{3}|\d{5})\)\s?(\d{5}|(\d{3}\s?\d{2}\s?\d{2}))$/i', 'message' => 'Пожалуйста, укажите номер телефона в формате +7 (495) 363 1111'],
            [['phone'], 'string', 'min' => 11, 'max' => 24],

            /*[['email'], 'email', 'pattern' => '/[a-zA-Zа-яА-Я0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Zа-яА-Я0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Zа-яА-Я0-9](?:[a-zA-Zа-яА-Я0-9-]*[a-zA-Zа-яА-Я0-9])?\.)+[a-zA-Zа-яА-Я0-9](?:[a-zA-Zа-яА-Я0-9-]*[a-zA-Zа-яА-Я0-9])?/', 'fullPattern' => '/^[^@]*<[a-zA-Zа-яА-Я0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Zа-яА-Я0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Zа-яА-Я0-9](?:[a-zA-Zа-яА-Я0-9-]*[a-zA-Zа-яА-Я0-9])?\.)+[a-zA-Zа-яА-Я0-9](?:[a-zA-Zа-яА-Я0-9-]*[a-zA-Zа-яА-Я0-9])?>$/', 'message' => 'Пожалуйста, укажите корректный e-mail в формате user@server.com'],*/
            [['email'], 'email',  'message' => 'Пожалуйста, укажите корректный e-mail в формате user@server.com'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand_name' => 'Название брэнда',
//            'company_name' => 'Название компании',
            'nomination_id' => 'Номинация',
            'seminar' => 'Участвовать в семинаре',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'job' => 'Должность',
            'phone' => 'Номер телефона',
            'email' => 'Электронная почта',
            'reg_date' => 'Дата регистрации',
            'status' => 'Статус заявки'
        ];
    }

    public function getNomination()
    {
        return self::hasOne(Nomination::className(), ['id' => 'nomination_id']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->status != $this->getOldAttribute('status')) {
                if ($this->status == 1) {

                    return  \Yii::$app->mailer->compose('@common/mail/approve', ['participant' => $this])
                        ->setFrom(\Yii::$app->params['rbkEmail'])
                        ->setTo($this->email)
                        ->setSubject('Заявка на регистрацию в конкурсе принята')
                        ->send();
                }
                if ($this->status == 2) {

                    return \Yii::$app->mailer->compose('@common/mail/notapprove', ['participant' => $this])
                        ->setFrom(\Yii::$app->params['rbkEmail'])
                        ->setTo($this->email)
                        ->setSubject('Заявка на участие в конкурсе отклонена')
                        ->send();
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
