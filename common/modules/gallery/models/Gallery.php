<?php

namespace common\modules\gallery\models;

use common\helpers\CDirectory;
use common\helpers\CString;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%gallery}}".
 *
 * @property integer $id
 * @property string $img_src
 * @property string $img
 * @property string $video
 * @property integer $position
 * @property integer $is_main
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gallery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['img_src', 'img', 'video'], 'required'],

            [['video'], 'string'],
            [['position', 'is_main'], 'integer'],
            [['img_src', 'img'], 'string', 'max' => 512],
            [['position'], 'default', 'value' => 99],
            [['img'], 'image', 'extensions' => 'jpg, jpeg, png, gif', 'skipOnEmpty' => true],
            [['img'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img_src' => 'Путь к файлу',
            'img' => 'Фото / Preview',
            'video' => 'Ссылка на видео',
            'position' => 'Позиция отображения',
            'is_main' => 'Главная',
        ];
    }

    public function behaviors()
    {
        return [
            'cache' => [
                'class' => 'common\behaviors\CachedImageResolution',
                'attr_src' => 'img_src',
                'attr_img_name' => 'img',
            ]
        ];
    }

    public function uploadPhoto()
    {

        $img = UploadedFile::getInstance($this, 'img');
        if ($img instanceof UploadedFile && $img->size > 0) {
            $this->img = $img;

            $path = 'images/modules/gallery/' . $this->getPrimaryKey();
            CDirectory::createDir($path);

            $dir = Yii::$app->basePath . "/../" . $path; //Yii::getAlias('@common/images/');
            $imageName = CString::translitTo($this->img) . "." . $this->img->getExtension();

            $this->img->saveAs($dir . "/" . $imageName);

            Gallery::updateAll(['img' => $imageName, 'img_src' => $path], ['id' => $this->getPrimaryKey()]);

        } else {
//            $old = $this->getOldAttribute('img');
////            $old_src = $this->getOldAttribute('img_src');
//            $this->img = ($old == null ? '' : $old);
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $old = $this->getOldAttribute('img');
            $this->img = ($old == null ? '' : $old);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->uploadPhoto();

        if ($this->is_main) {
            Gallery::updateAll(['is_main' => 0], 'id <> ' . $this->getPrimaryKey());
        }


        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deletePhoto();
            return true;
        } else {
            return false;
        }
    }

    public function deletePhoto()
    {
        if (is_file(Yii::$app->basePath . "/../" . $this->img_src . "/" . $this->img)) {
            $result = unlink(Yii::$app->basePath . "/../" . $this->img_src . "/" . $this->img);
            FileHelper::removeDirectory(Yii::$app->basePath . "/../cache/" . $this->img_src);
            FileHelper::removeDirectory(Yii::$app->basePath . "/../" . $this->img_src);
            return $result;
        }
        return false;
    }

    public function videoUrl()
    {
        if (strpos($this->video, 'www.youtube.com')) {
            $href = str_replace('/watch?v=', '/embed/', $this->video);
            $href .= '?autoplay=1';
            return $href;
        } else {
            $href = str_replace('autoStart=false', 'autoStart=true', $this->video);
            return $href;
        }
    }
}
