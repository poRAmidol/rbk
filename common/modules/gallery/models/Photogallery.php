<?php

namespace common\modules\gallery\models;

use Yii;

/**
 * This is the model class for table "{{%photogallery}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 */
class Photogallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photogallery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description', 'seo_description'], 'string'],
            [['name', 'seo_title', 'seo_keywords'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'seo_title' => 'SEO Заголовок',
            'seo_keywords' => 'SEO Ключевые слова',
            'seo_description' => 'SEO Описание',
        ];
    }

    public function getPhotos()
    {
        return self::hasMany(Photo::className(), ['gallery_id' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->photos as $_p) {
                $_p->delete();
            }
            return true;
        } else {
            return false;
        }

    }
}
