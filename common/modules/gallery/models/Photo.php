<?php

namespace common\modules\gallery\models;

use common\helpers\CDirectory;
use common\helpers\CString;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%photo}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $src
 * @property integer $gallery_id
 * @property integer $is_main
 */
class Photo extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['gallery_id', 'is_main'], 'integer'],
            [['src'], 'string', 'max' => 512],
            [['name'], 'image', 'extensions' => 'jpg, gif, jpeg, png', 'skipOnEmpty' => true, 'maxFiles' => 10],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название фото',
            'src' => 'Путь к фото',
            'gallery_id' => 'Галерея',
            'is_main' => 'Отобразить фото на главной',
            'file' => 'Название фото'
        ];
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\CachedImageResolution',
                'attr_src' => 'src',
                'attr_img_name' => 'name',
            ]
        ];
    }

    public function uploadAndSavePhoto(Photogallery $model) {
        if (!empty($this->name))
        foreach ($this->name as $_file) {

            if ($_file instanceof UploadedFile && $_file->size > 0) {


                $path = 'images/modules/gallery/'.$model->getPrimaryKey();
                CDirectory::createDir($path);
                $dir = Yii::$app->basePath."/../".$path;

                $imageName = md5(CString::translitTo($_file->name) . microtime()) . "." . $_file->getExtension();
                $_file->saveAs($dir."/".$imageName);

                $photo = new Photo();
                $photo->name = $imageName;
                $photo->src = $path;
                $photo->gallery_id = $model->getPrimaryKey();
                $photo->save();
            }
        }

    }
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deletePhoto();
            return true;
        } else {
            return false;
        }
    }
    public function deletePhoto() {
        if (is_file(Yii::$app->basePath."/../".$this->src."/".$this->name)) {
            unlink(Yii::$app->basePath."/../".$this->src . "/" . $this->name);
            FileHelper::removeDirectory(Yii::$app->basePath."/../cache/".$this->src);
        }
    }
}
