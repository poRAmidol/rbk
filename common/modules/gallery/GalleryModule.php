<?php

namespace common\modules\gallery;

class GalleryModule extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\gallery\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
